// Restricts input for the set of matched elements to the given inputFilter function.
(function ($) {
  $.fn.inputFilter = function (inputFilter) {
    return this.on(
      "input keydown keyup mousedown mouseup select contextmenu drop",
      function () {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }
      }
    );
  };
})(jQuery);

function LoadingShow() {
  $.LoadingOverlay("show");
}

function LoadingHide() {
  $.LoadingOverlay("hide");
}

function phoneNumber(inputNumber) {
  var result = {
    success: "",
    message: "",
  };
  var phoneno = /^[0-9]{10}$/;
  var match = phoneno.exec(inputNumber);
  if (inputNumber.length == 10) {
    if (match != null && (inputNumber.substring(0, 1) == 0)) {
        result.success = true;
        result.message = "หมายเลขโทรศัพท์ถูกต้อง";
    } else {
        result.success = false;
        result.message = "หมายเลขโทรศัพท์ไม่ถูกต้อง";
    }
  } else {
    result.success = false;
    result.message = "จำนวนหมายเลขโทรศัพท์ไม่ถูกต้อง";
  }
  return JSON.stringify(result)
}
