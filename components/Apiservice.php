<?php

namespace app\components;

use Yii;
use yii\web\HttpException;
use yii\base\Component;
use yii\data\ArrayDataProvider;
use yii\httpclient\Client;
use yii\rest\Request;
use yii\helpers\Json;

class Apiservice extends Component
{
    // API NODEJS
    public function sendApiNodeService($function = null, $datas = null, $method = 'GET')
    {
        // echo '<pre>';print_r($datas);exit;
        if (empty($function)) {
            return ['success' => false, 'error' => 'กรุณาตรวจสอบตัวแปรให้สมบูรณ์!'];
        } else {
            $client = new Client(['baseUrl' => Yii::$app->params['apiNodeBaseUrl']]);
            $response = $client->createRequest()
                ->setMethod($method)
                ->setFormat('json')
                ->setUrl($function)
                ->addHeaders(['app_code' => Yii::$app->params['apiNodeAppCode']])
                ->setData($datas)
                ->send();
            $result = json_decode($response->content, true);
            return $result;
        }
    }
    // API
    public function sendDataKbank($data)
    {
        $ch = curl_init();
        $data_json = json_encode($data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Cache-Control:no-cache',
            'x-api-key: ' . Yii::$app->params['secretkey']
        ));
        curl_setopt($ch, CURLOPT_URL, Yii::$app->params['urlChargeCard']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_SSLVERSION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);

        $data = curl_exec($ch);
        $response = json_decode($data);

        curl_close($ch);

        $result = json_decode(json_encode($response), True);

        return $result;
    }
    
    public function sendGetOrderID($data)
    {
        $ch = curl_init();
        $data_json = json_encode($data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Cache-Control:no-cache',
            'x-api-key: ' . Yii::$app->params['secretkey']
        ));
        curl_setopt($ch, CURLOPT_URL, "https://dev-kpaymentgateway-services.kasikornbank.com/qr/v2/order");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_SSLVERSION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);

        $data = curl_exec($ch);
        $response = json_decode($data);

        curl_close($ch);

        $result = json_decode(json_encode($response), True);

        return $result;
    }

}
