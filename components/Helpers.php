<?php

namespace app\components;
use Yii;
use yii\base\Component;
use yii\data\ArrayDataProvider;

class Helpers extends Component
{

    public static function convCode($str, $conv = false)
    {
        if (!$conv) {
            return @iconv('tis-620', 'utf-8', trim($str));
        } else {
            return @iconv('utf-8', 'tis-620', trim($str));
        }
    }

    public static function formatDateMedium($datetime = false, $showTime = false, $dateWidth = 'long')
    {
        if ($datetime === false) {
            return !$showTime ? Yii::app()->dateFormatter->formatDateTime(time(), $dateWidth, false) : Yii::app()->dateFormatter->formatDateTime(time(), $dateWidth);
        } elseif ($datetime === null || empty($datetime)) {
            return null;
        } else {
            $datetime = str_replace('/', '-', $datetime);
            return !$showTime ? Yii::app()->dateFormatter->formatDateTime(strtotime($datetime), $dateWidth, false) : Yii::app()->dateFormatter->formatDateTime(strtotime($datetime), $dateWidth);
        }
    }

    public static function thaiFormatDate($dateString = '', $format = 'd/m/Y')
    {
        $dateString = $dateString != '' ? date('Y-m-d H:i:s', strtotime($dateString)) : date('Y-m-d H:i:s');
        $dt = new \DateTime($dateString);
        $dt->modify('+ 543 years');
        return $dt->format($format);
    }

    public static function excelFormatDate($dateString = '', $fromFormat = 'd/m/Y', $toFormat = 'Y-m-d\T00:00:00:000', $fromFormat2 = 'm-d-y')
    {
        if ($dateString == '') return '';

        $dt = date_create_from_format($fromFormat, $dateString);
        if (!$dt) {
            $dt = date_create_from_format($fromFormat2, $dateString);
        }
        return date_format($dt, $toFormat);
    }

    public static function substrToIIA($str, $maxlength = 50, $strEnd = '...')
    {
        if (mb_strlen($str, Yii::$app->charset) > $maxlength) {
            return mb_substr($str, 0, ($maxlength - strlen($strEnd)), Yii::$app->charset) . $strEnd;
        }
        return $str;
    }

    public static function listMonth($language = 'TH', $format = 'L', $placeholder = false, $toChart = false)
    {
        $arrMonth = array();
        if ($language == 'TH') {

            if ($format == 'L') {
                $arrMonth = array('1' => 'มกราคม', '2' => 'กุมภาพันธ์', '3' => 'มีนาคม', '4' => 'เมษายน', '5' => 'พฤษภาคม', '6' => 'มิถุนายน', '7' => 'กรกฎาคม', '8' => 'สิงหาคม', '9' => 'กันยายน', '10' => 'ตุลาคม', '11' => 'พฤศจิกายน', '12' => 'ธันวาคม');
            }

            if ($format == 'S') {
                $arrMonth = array('1' => 'ม.ค.', '2' => 'ก.พ.', '3' => 'มี.ค.', '4' => 'เม.ย.', '5'      => 'พ.ค.', '6' => 'มิ.ย.', '7' => 'ก.ค.', '8' => 'ส.ค.', '9' => 'ก.ย.', '10' => 'ต.ค.', '11' => 'พ.ย.', '12' => 'ธ.ค.');
            }
        }

        if ($language == 'EN') {

            if ($format == 'L') {
                $arrMonth = array('1' => 'January', '2' => 'February', '3' => 'March', '4' => 'April', '5' => 'May', '6' => 'June', '7' => 'July', '8' => 'August', '9' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
            }

            if ($format == 'S') {
                $arrMonth = array('1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'May', '6' => 'Jun', '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
            }
        }

        if ($placeholder) {
            $arrMonth = ['00' => 'ระบุเดือน'] + $arrMonth;
        }

        if ($toChart) {
            $j = 0;
            for ($i = 0; $i < 12; $i++) {
                $j = $i + 1;
                $arrToChart[$i] = $arrMonth[$j];
            }
            unset($arrMonth);
            $arrMonth = $arrToChart;
        }

        return $arrMonth;
    }

    public static function listYear($intDecrease = 0, $intIncrease = 0, $placeholder = false)
    {
        $intYearStart = intval(date('Y'));
        $intYearEnd   = intval(date('Y')) + $intIncrease;

        if ($intDecrease > 0) {
            $intYearStart = $intYearStart - $intDecrease;
        }

        $arrYear = array();
        for ($i = $intYearStart; $i <= $intYearEnd; $i++) {
            $arrYear += [$i => $i];
        }

        if ($placeholder) {
            $arrYear = [0 => 'ระบุปี'] + $arrYear;
        }

        return $arrYear;
    }

    public static function getDateTHDMY($Date, $symbol)
    {
        $arrMonthTH = Helpers::listMonth('TH', 'L', false);

        if (($Date != "") && (strlen(trim($Date)) >= 8)) {
            $subDate = explode($symbol, $Date);
            $day     = $subDate[0];
            $month   = $arrMonthTH[intval($subDate[1])];
            $year    = intval($subDate[2]) + 543;

            return $day . " " . $month . " " . $year;
        } else {
            return "-";
        }
    }

    public static function getDateTHDMYByFormat($Date, $symbol, $format = 'L')
    {
        $arrMonthTH = Helpers::listMonth('TH', $format, false);

        if (($Date != "") && (strlen(trim($Date)) >= 8)) {
            $subDate = explode($symbol, $Date);
            $day     = $subDate[0];
            $month   = $arrMonthTH[intval($subDate[1])];
            $year    = intval($subDate[2]) + 543;

            return $day . " " . $month . " " . substr($year, -2);
        } else {
            return "-";
        }
    }

    public static function getFullDateTHDMY($date, $symbol)
    {
        $arrMonthTH = Helpers::listMonth('TH', 'L', false);

        if (!empty($date)) {

            $datetime = explode(' ', $date);
            $datesep = explode($symbol, $datetime[0]);

            $time = explode(".", $datetime[1]);

            $datereturn = $datesep[2] . " " . $arrMonthTH[intval($datesep[1])] . " " . intval($datesep[0] + 543) . " " . $time[0];
        } else {
            $datereturn = " - ";
        }
        return $datereturn;
    }

    public static function getDateYMDtoDMY($Date, $symbol)
    {
        if (($Date != "") && (strlen(trim($Date)) >= 8)) {
            $subDate = explode($symbol, $Date);
            $day = $subDate[2];
            $month = $subDate[1];
            $year = $subDate[0];
            $strDate = $day . "/" . $month . "/" . $year;

            return $strDate;
        } else {
            return "-";
        }
    }

    public static function getDateYMDtoDMYTH($Date, $symbol)
    {
        if (($Date != "") && (strlen(trim($Date)) >= 8)) {
            $subDate = explode($symbol, $Date);
            $day     = $subDate[2];
            $month   = $subDate[1];
            $year    = $subDate[0];
            $strDate = $day . "/" . $month . "/" . $year;

            return Helpers::getDateTHDMY($strDate, "/");
        } else {
            return "-";
        }
    }

    public static function convMYSqlDateDMYtoYMD($Date, $symbol)
    {
        if (($Date !== '') && (strlen(trim($Date)) >= 8)) {
            $subDate = explode($symbol, $Date);
            if (count($subDate) == 3) {
                $day     = trim($subDate[0]);
                $month   = trim($subDate[1]);
                $year    = trim($subDate[2]);
                if (intval($year) > intval(date('Y') + 500)) {
                    $year = intval($year) - 543;
                }
                $strDate = $year . "-" . $month . "-" . $day;

                return $strDate;
            }
            return '';
        } else {
            return '';
        }
    }

    public static function getReplaceMemoText($Text)
    {
        $str = str_replace(" ", "&nbsp;", str_replace(chr(13), "<br>", $Text));
        return $str;
    }

    public static function con_date($datetime)
    {
        if (trim($datetime) == "") {
            return "";
        }
        $datetime = trim($datetime);
        $dt = explode(" ", $datetime);
        $date = explode("-", $dt[0]);
        $time = isset($dt[1]) ? $dt[1] : "";
        if (isset($time) && $time != "") {
            return $date[2] . "-" . $date[1] . "-" . $date[0] . " " . $time;
        } else {
            return $date[2] . "-" . $date[1] . "-" . $date[0];
        }
    }

    public static function thaiAmount($num)
    {
        $num = str_replace(',', '', $num);
        $th_num = array('ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า');
        $th_pos = array('1' => 'หน่วย', '2' => 'สิบ', '3' => 'ร้อย', '4' => 'พัน', '5' => 'หมื่น', '0' => 'แสน', '7' => 'ล้าน');
        $txt = '';

        $arr_num = str_split($num);
        $int_len = count($arr_num);
        $decimal = false;
        $arr_decimal = array();

        if (in_array('.', $arr_num)) {
            $decimal_pos = array_search('.', $arr_num);
            $start_decimal_pos = $int_len - ($decimal_pos + 1);

            for ($i = ($decimal_pos + 1); $i < $int_len; $i++) {
                $arr_decimal[$start_decimal_pos] = $arr_num[$i];
                $start_decimal_pos--;
            }

            $int_len = $decimal_pos;
            $decimal = true;
        }

        foreach ($arr_num as $val) {
            $arr_int[$int_len] = $val;
            $int_len--;
            if ($int_len == 0)
                break;
        }

        foreach ($arr_int as $key => $val) {
            $num_pos = $key % 6;
            if ($val == 0) {
                if ($key > 6 && $num_pos == 1)
                    $txt .= 'ล้าน';
                else
                    continue;
            } else {
                if ($val == 1 and $num_pos == 2) {
                    $txt .= 'สิบ';
                } else if ($val == 2 and $num_pos == 2) {
                    $txt .= 'ยี่สิบ';
                } else if ($num_pos != 1) {
                    $txt .= $th_num[$val] . $th_pos[$num_pos];
                } else {
                    if (count($arr_int) > 1 && $val == 1 && $num_pos == 1 && isset($arr_int[$key + 1]) && $arr_int[$key + 1] > 0)
                        $txt .= 'เอ็ด';
                    else
                        $txt .= $th_num[$val];
                }

                if ($key > 6 && $num_pos == 1) {
                    $txt .= 'ล้าน';
                }
            }

            $prev_num = $val;
        }

        $txt .= 'บาท';

        $countDecimal = 0;
        foreach ($arr_decimal as $key => $val) {
            if ($val == 0) {
                continue;
            } else {
                if ($val == 1 and $key == 2) {
                    $txt .= 'สิบ';
                } else if ($val == 2 and $key == 2) {
                    $txt .= 'ยี่สิบ';
                } else if ($key > 1) {
                    $txt .= $th_num[$val] . $th_pos[$key];
                } else {
                    $txt .= $th_num[$val];
                }

                $countDecimal++;
            }
        }

        if ($countDecimal > 0) {
            $txt .= 'สตางค์';
        } else {
            $txt .= 'ถ้วน';
        }

        return $txt;
    }

    public static function convert_datedb_to_string($numdate)
    {
        $arrMonthTH = Helpers::listMonth('TH', 'L', false);

        $dateresault = explode("-", substr($numdate, 0, -13));
        $timeresault = explode(" ", substr($numdate, 0, -4));
        $dateretern = $dateresault[2] . " " . $arrMonthTH[intval($dateresault[1])] . " " . ($dateresault[0] + 543) . " " . $timeresault[1];
        if (($dateresault[0] + 543) > 2500) {
            return $dateretern;
        } else {
            return "";
        }
    }

    public static function getChecked($RowValue, $strValue)
    {
        if ($RowValue == $strValue) {
            return "checked";
        } else {
            return "";
        }
    }

    public static function arrayquery_to_array($resultQuery)
    {
        $arrVal = array();
        foreach ($resultQuery as $valResult) {
            array_push($arrVal, implode(',', $valResult));
        }
        return $arrVal;
    }

    public static function dataActiverecordtoDataProvider($dataActiverecord, $pagination = 20)
    {
        $arrColumn = array();

        foreach ($dataActiverecord as $valResult) {
            $arrColumn = array_keys($valResult);
        }

        if (!$pagination) {
            $dataProvider = new ArrayDataProvider([
                'allModels' => $dataActiverecord,
                'pagination' => false,
                'sort' => [
                    'attributes' => $arrColumn,
                ],
            ]);
        } else {
            $dataProvider = new ArrayDataProvider([
                'allModels' => $dataActiverecord,
                'pagination' => [
                    'pageSize' => $pagination,
                ],
                'sort' => [
                    'attributes' => $arrColumn,
                ],
            ]);
        }
        return $dataProvider;
    }

    public static function DateDiff($strDate1, $strDate2)
    {
        return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
    }

    public static function TimeDiff($strTime1, $strTime2)
    {
        return (strtotime($strTime2) - strtotime($strTime1)) / (60 * 60); // 1 Hour =  60*60
    }

    // $time1 = new DateTime('2017-01-23 18:16:25');
    // $time2 = new DateTime('2019-01-23 11:36:28');
    public static function getTimeDiff($strTime1, $strTime2)
    {
        if (!empty($strTime1) && !empty($strTime2)) {
            $time1 = new \DateTime($strTime1);
            $time2 = new \DateTime($strTime2);
            $timediff = $time1->diff($time2);
            return $timediff->format('%h/%i');
        }
        return '';
    }

    public static function checkNumber($numberValue)
    {
        if (trim($numberValue) == '') {
            return 0;
        } else {
            return $numberValue;
        }
    }

    public static function ReplaceCarLicense($licenseno)
    {
        return trim(str_replace(' ', '', str_replace('-', '', $licenseno)));
    }

    public static function numberFormat($number, $decimal = NULL)
    {
        if (strstr($number, '.')) {
            $numbers = explode('.', $number);

            if ($decimal && $numbers[1] <> 0)
                $number = number_format($numbers[0] . '.' . $numbers[1], $decimal);
            else
                $number = number_format($numbers[0]);
        } else {
            if ($decimal && $number <> 0)
                $number = number_format($number, $decimal);
            else
                $number = number_format($number);
        }

        return $number;
    }

    public static function PhoneFormat($phone_number)
    {
        $phone_number = str_replace('-', '', $phone_number);
        if (strlen($phone_number > 8)) {
            $number_1 = substr($phone_number, 0, 3);
            $number_2 = substr($phone_number, 3, 3);
            $number_3 = substr($phone_number, 6);
            $format_number = $number_1 . '-' . $number_2 . '-' . $number_3;
        } else {
            $format_number = $phone_number;
        }

        return $format_number;
    }

    // ตรวจสอบเบอร์มือถือ ตัวแรกต้องเป็น 0 เท่านั้น และครบ 10 หลัก
    public function mobileNumber($txt)
    {
        if (!preg_match('/^[0-9]{10}$/', $txt) || substr($txt, 0, 1) != 0) {
            return 'เบอร์โทรศัพท์ไม่ถูกต้อง';
        }
    }

    public static function SortNumber($array)
    {
        //Logic Qiuck Sort
        if (count($array) == 0) {
            return array();
        }

        $pivot = $array[0];
        $left  = $right = array();

        for ($i = 1; $i < count($array); $i++) {
            if ($array[$i] < $pivot)
                $left[] = $array[$i];
            else
                $right[] = $array[$i];
        }

        return array_merge(krsort($left), krsort(array($pivot)), krsort($right));
    }

    public static function getWeeksofMonth($date, $rollover = 'sunday')
    {
        $cut = substr($date, 0, 8);
        $daylen = 86400;

        $timestamp = strtotime($date);
        $first = strtotime($cut . "00");
        $elapsed = ($timestamp - $first) / $daylen;

        $weeks = 1;

        for ($i = 1; $i <= $elapsed; $i++) {
            $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
            $daytimestamp = strtotime($dayfind);

            $day = strtolower(date("l", $daytimestamp));

            if ($day == strtolower($rollover))  $weeks++;
        }

        return $weeks;
    }

    // ฟังก์ชันแยกข้อความ error จาก Model
    public static function GetErrorModel($model, $separate = ', ', $runNum = false)
    {
        $arrError = [];
        $errorNum = count($model->getErrors()); // จำนวนข้อมูลที่ error ทั้งหมด
        $rowNum = 0;
        foreach ($model->getErrors() as $key => $value) {
            $arrError[] = (($errorNum > 1) ? (empty($runNum) ? '' : ++$rowNum . '. ') : '') . $value[0];
        }
        if (!empty($arrError)) {
            return implode($separate, $arrError);
        }
        return $arrError;
    }

    public static function PrintException($e)
    {
        return $e->getMessage() . ' (' . $e->getFile() . ':' . $e->getLine() . ")\n";
    }
    public static function getProcessingTime($date1, $date2 = null, $lang = 'TH')
    {
        $date1 = date('Y-m-d', strtotime($date1));
        $date2 = empty($date2) ? date('Y-m-d') : date('Y-m-d', strtotime($date2));

        list($byear, $bmonth, $bday) = explode('-', $date1);
        list($tyear, $tmonth, $tday) = explode('-', $date2);

        if ($byear < 1970) {
            $yearad = 1970 - $byear;
            $byear = 1970;
        } else {
            $yearad = 0;
        }

        $mbirth = mktime(0, 0, 0, $bmonth, $bday, $byear);
        $mtoday = mktime(0, 0, 0, $tmonth, $tday, $tyear);

        $mage = ($mtoday - $mbirth);
        $wyear = (date('Y', $mage) - 1970 + $yearad);
        $wmonth = (date('m', $mage) - 1);
        $wday = (date('d', $mage) - 1);

        if ($lang == 'TH') {
            $ystr = ' ปี';
            $mstr = ' เดือน';
            $dstr = ' วัน';
        } else {
            $ystr = ($wyear > 1 ? " Years" : " Year");
            $mstr = ($wmonth > 1 ? " Months" : " Month");
            $dstr = ($wday > 1 ? " Days" : " Days");
        }

        if ($wyear > 0 && $wmonth > 0 && $wday > 0) {
            $agestr = $wyear . $ystr . " " . $wmonth . $mstr . " " . $wday . $dstr;
        } else if ($wyear == 0 && $wmonth == 0 && $wday > 0) {
            $agestr = $wday . $dstr;
        } else if ($wyear > 0 && $wmonth > 0 && $wday == 0) {
            $agestr = $wyear . $ystr . " " . $wmonth . $mstr;
        } else if ($wyear == 0 && $wmonth > 0 && $wday > 0) {
            $agestr = $wmonth . $mstr . " " . $wday . $dstr;
        } else if ($wyear > 0 && $wmonth == 0 && $wday > 0) {
            $agestr = $wyear . $ystr . " " . $wday . $dstr;
        } else if ($wyear == 0 && $wmonth > 0 && $wday == 0) {
            $agestr = $wmonth . $mstr;
        } else {
            $agestr = "0";
        }

        return $agestr;
    }

    public static function formatMobileNumberToSMS($number)
    {
        $number = preg_replace('/^0/', '66', str_replace('-', '', $number));

        return $number;
    }

    public static function xmlToObject($xml_string)
    {
        return self::parseXML(simplexml_load_string($xml_string));
    }

    public static function parseXML($xml)
    {
        $result_xml = new \stdclass();
        $ns = $xml->getNamespaces(true);
        $ns = array_unique($ns);
        $ns[''] = '';
        foreach ($ns as $name => $url) {
            foreach ($xml->attributes($ns[$name]) as $attr_name => $attr_value) {
                $result_xml->{$attr_name} = (string) $attr_value;
            }
        }

        foreach ($ns as $name => $url) {
            $childs = $xml->children($ns[$name]);
            foreach ($childs as $child_name => $child_xml) {
                if (isset($result_xml->{$child_name})) {
                    $result_xml->{$child_name} = is_array($result_xml->{$child_name}) ? $result_xml->{$child_name} : array($result_xml->{$child_name});
                    $result_xml->{$child_name}[] = trim($child_xml->__toString()) ? trim($child_xml->__toString()) : self::parseXML($child_xml);
                } else {
                    if ($child_name == 'Text') {
                        $result_xml->{$child_name} = trim($child_xml->__toString());
                    } else {
                        $result_xml->{$child_name} = ((@count($child_xml->children()) == 0) && (@count($child_xml->attributes()) == 0))
                            ? trim($child_xml->__toString()) : self::parseXML($child_xml);
                        if ((@count($child_xml->children()) == 0) && (@count($child_xml->attributes()) > 0) && trim($child_xml->__toString())) {
                            $result_xml->{$child_name}->value = trim($child_xml->__toString());
                        }
                    }
                }
            }
        }
        return $result_xml;
    }

    public static function clickToCallXLite($tel, $name = null)
    {
        // https://10.26.192.11/click2call/click2dial.php?extension=2551&phone=[PHONE]&name=[CUSTNAME/ETC.]
        $extension = Yii::$app->user->identity->call_extension;
        $res = null;
        if (!empty($tel)) {
            $res = $tel;
            if (!empty($extension)) {
                if (!empty($name)) {
                    $name = str_replace(' ', '_', trim($name));
                }
                $url = 'https://10.26.192.11/click2call/click2dial.php?extension=' . $extension . '&phone=' . str_replace('-', '', $tel) . '&name=' . $name;
                $res = '<a href="javascript:void(0);" data-url="' . $url . '" onclick="clickToCallXLite(this);">' . $tel . '</a>';
            }
        }

        return $res;
    }

    public static function uploadFileFtp($ftpHost, $ftpPort, $ftpUsername, $ftpPassword, $files, $dirName, $dirNameThumb = '', $resize_width = null, $resize_height = null)
    {
        $arrFileType = ['image/png', 'image/gif', 'image/jpeg', 'image/pjpeg'];
        $check_size  = 1208;
        $dataReturn  = array();
        $dataError   = array();
        if (is_array($files["name"])) {
            // MULTIPLE FILE
            if (!empty($files["name"][0])) {
                // setup of connection
                $conn = ftp_connect($ftpHost, $ftpPort) or die("could not connect to " . $ftpHost . ":" . $ftpPort);
                ftp_login($conn, $ftpUsername, $ftpPassword);
                ftp_pasv($conn, true);
                Helpers::ftp_mksubdirs($conn, "/", $dirName);
                if (!empty($dirNameThumb)) {
                    Helpers::ftp_mksubdirs($conn, "/", $dirNameThumb);
                }
                for ($i = 0; $i < count($files["name"]); $i++) {

                    if (!empty($files["tmp_name"][$i])) {
                        $fileInfo    = pathinfo($files["name"][$i]);
                        $oldFileName = $fileInfo["filename"] . '.' . $fileInfo["extension"];
                        $fileName    = md5($fileInfo["filename"] . time()) . '.' . $fileInfo["extension"];
                        $pathUpload  = "/" . $dirName . '/' . $fileName;

                        // fILE UPLOAD CHECK FILENAME IMG
                        if (in_array($files["type"][$i], $arrFileType)) {
                            // ORGINAL WIDTH/HEIGHT
                            list($orig_width, $orig_height) = getimagesize($files["tmp_name"][$i]);
                            $ImageType = '';
                            switch ($files["type"][$i]) {
                                case 'image/png':
                                    $ImageType = 'imagepng';
                                    $image_res =  Helpers::LoadPNG($files["tmp_name"][$i]);
                                    break;
                                case 'image/gif':
                                    $ImageType = 'imagegif';
                                    $image_res =  imagecreatefromgif($files["tmp_name"][$i]);
                                    break;
                                case 'image/jpeg':
                                case 'image/pjpeg':
                                    $ImageType = 'imagejpeg';
                                    $image_res = imagecreatefromjpeg($files["tmp_name"][$i]);
                                    break;
                            }

                            if (!empty($resize_width) && !empty($resize_height)) {
                                if ($orig_width == $resize_width || $orig_height == $resize_height) {
                                    ftp_put($conn, $pathUpload, $files["tmp_name"][$i], FTP_BINARY); //SAVE
                                } else {
                                    // RESIZE IMAGE
                                    $resizeImage = Helpers::ftpResizeImg($image_res, $files["tmp_name"][$i], $orig_width, $orig_height, $resize_width, $resize_height);
                                    if ($resizeImage) {
                                        if ($ImageType($resizeImage, $files["name"][$i])) {
                                            if (ftp_put($conn, $pathUpload, $files["name"][$i], FTP_BINARY))  //SAVE
                                            {
                                                unlink($files["name"][$i]);
                                            }
                                        }
                                    }
                                }
                            } else if ($orig_width > $check_size) // CHECK WIDTH
                            {
                                // RESIZE IMAGE
                                $resizeImage = Helpers::ftpResizeImg($image_res, $files["tmp_name"][$i], $orig_width, $orig_height, $resize_width, $resize_height);
                                if ($resizeImage) {
                                    if ($ImageType($resizeImage, $files["name"][$i])) {
                                        if (ftp_put($conn, $pathUpload, $files["name"][$i], FTP_BINARY))  //SAVE
                                        {
                                            unlink($files["name"][$i]);
                                        }
                                    }
                                }
                            } else {
                                if ($orig_height > $check_size) // CHECK HEIGHT
                                {
                                    // RESIZE IMAGE
                                    $resizeImage = Helpers::ftpResizeImg($image_res, $files["tmp_name"][$i], $orig_width, $orig_height, $resize_width, $resize_height);
                                    if ($resizeImage) {
                                        if ($ImageType($resizeImage, $files["name"][$i])) {
                                            if (ftp_put($conn, $pathUpload, $files["name"][$i], FTP_BINARY))  //SAVE
                                            {
                                                unlink($files["name"][$i]);
                                            }
                                        }
                                    }
                                } else {
                                    ftp_put($conn, $pathUpload, $files["tmp_name"][$i], FTP_BINARY); //SAVE
                                }
                            }
                        } else {
                            ftp_put($conn, $pathUpload, $files["tmp_name"][$i], FTP_BINARY); //SAVE 
                        }
                        //CREATE DATA SAVE
                        $imageUrl = $dirName . '/';
                        $imageFtp = 'ftp://' . $ftpUsername . ':' . $ftpPassword . '@' . $ftpHost . '/' . ($dirName . $fileName);
                        $imageThumbUrl = '';
                        if (!empty($dirNameThumb)) {
                            $imageThumbUrl = 'ftp://' . $ftpUsername . ':' . $ftpPassword . '@' . $ftpHost . '/' . ($dirNameThumb . $fileName);
                        }
                        array_push($dataReturn, array(
                            "imageUrl"      => $imageUrl,
                            "imageFtp"      => $imageFtp,
                            "imageUrlThumb" => $imageThumbUrl,
                            "imageFtpThumb" => $imageThumbUrl,
                            "imageName"     => $fileName,
                            "imageOldName"  => $oldFileName,
                        ));
                    } else {
                        //ERROR IMG
                        array_push($dataError, array(
                            "imageName" => $files["tmp_name"][$i],
                            "error"     => true,
                        ));
                    }
                }
                // FTP CLOSE
                ftp_close($conn);
            }
        } else {
            // SINGLE FILE
            if (!empty($files["name"])) {
                // setup of connection
                $conn = ftp_connect($ftpHost, $ftpPort) or die("could not connect to " . $ftpHost . ":" . $ftpPort);
                ftp_login($conn, $ftpUsername, $ftpPassword);
                ftp_pasv($conn, true);
                Helpers::ftp_mksubdirs($conn, "/", $dirName);
                if (!empty($dirNameThumb)) {
                    Helpers::ftp_mksubdirs($conn, "/", $dirNameThumb);
                }

                $fileInfo    = pathinfo($files["name"]);
                $oldFileName = $fileInfo["filename"] . '.' . $fileInfo["extension"];
                $fileName    = md5($fileInfo["filename"] . time()) . '.' . $fileInfo["extension"];
                $pathUpload  = "/" . $dirName . '/' . $fileName;
                if (!empty($files["tmp_name"])) {
                    // fILE UPLOAD CHECK FILENAME IMG
                    if (in_array($files["type"], $arrFileType)) {
                        // FILENAME EXTENSION
                        $ImageType = '';
                        switch ($files["type"]) {
                            case 'image/png':
                                $ImageType = 'imagepng';
                                $image_res =  Helpers::LoadPNG($files["tmp_name"]);
                                break;
                            case 'image/gif':
                                $ImageType = 'imagegif';
                                $image_res =  imagecreatefromgif($files["tmp_name"]);
                                break;
                            case 'image/jpeg':
                            case 'image/pjpeg':
                                $ImageType = 'imagejpeg';
                                $image_res = imagecreatefromjpeg($files["tmp_name"]);
                                break;
                        }
                        // ORGINAL WIDTH/HEIGHT
                        list($orig_width, $orig_height) = getimagesize($files["tmp_name"]);
                        // RESIZE IMAGE
                        $resizeImage = Helpers::ftpResizeImg($image_res, $files["tmp_name"], $orig_width, $orig_height);
                        if ($orig_width > $check_size) // CHECK WIDTH
                        {
                            if ($resizeImage) {
                                if ($ImageType($resizeImage, $files["name"])) {
                                    if (!empty($files["name"])) {
                                        if (ftp_put($conn, $pathUpload, $files["name"], FTP_BINARY)) //SAVE
                                        {
                                            unlink($files["name"]);
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($orig_height > $check_size) // CHECK HEIGHT
                            {
                                if ($resizeImage) {
                                    if ($ImageType($resizeImage, $files["name"])) {
                                        if (!empty($files["name"])) {
                                            if (ftp_put($conn, $pathUpload, $files["name"], FTP_BINARY))  //SAVE
                                            {
                                                unlink($files["name"]);
                                            }
                                        }
                                    }
                                }
                            } else {
                                ftp_put($conn, $pathUpload, $files["tmp_name"], FTP_BINARY); //SAVE
                            }
                        }
                    } else {
                        ftp_put($conn, $pathUpload, $files["tmp_name"], FTP_BINARY); //SAVE
                    }
                    //CREATE ARRDATA SAVE
                    $imageUrl = $dirName . '/';
                    $imageFtp = 'ftp://' . $ftpUsername . ':' . $ftpPassword . '@' . $ftpHost . '/' . ($dirName . $fileName);
                    $imageThumbUrl = '';
                    if (!empty($dirNameThumb)) {
                        $imageThumbUrl = 'ftp://' . $ftpUsername . ':' . $ftpPassword . '@' . $ftpHost . '/' . ($dirNameThumb . $fileName);
                    }
                    $dataReturn["imageUrl"]      = $imageUrl;
                    $dataReturn["imageFtp"]      = $imageFtp;
                    $dataReturn["imageUrlThumb"] = $imageThumbUrl;
                    $dataReturn["imageFtpThumb"] = $imageThumbUrl;
                    $dataReturn["imageName"]     = $fileName;
                    $dataReturn["imageOldName"]  = $oldFileName;
                } else {
                    //ERROR IMG
                    $dataError['imageName'] = $files["tmp_name"];
                    $dataError['error']     = true;
                }
                // FTP CLOSE
                ftp_close($conn);
            }
        }
        $obj = json_decode(json_encode($dataReturn));
        return $obj;
    }

    public static function LoadPNG($imgname)
    {
        /* Attempt to open */
        $im = @imagecreatefrompng($imgname);

        /* See if it failed */
        if (!$im) {
            /* Create a blank image */
            $im  = imagecreatetruecolor(150, 30);
            $bgc = imagecolorallocate($im, 255, 255, 255);
            $tc  = imagecolorallocate($im, 0, 0, 0);

            imagefilledrectangle($im, 0, 0, 150, 30, $bgc);

            /* Output an error message */
            imagestring($im, 1, 5, 5, 'Error loading ' . $imgname, $tc);
        }

        return $im;
    }

    public static function ftpResizeImg($fileName, $fileTmp, $orig_width, $orig_height, $resize_width = null, $resize_height = null)
    {
        if (!empty($resize_width) && !empty($resize_height)) {
            $new_img = imagecreatetruecolor($resize_width, $resize_height);
            $image   = $fileName;
            imagecopyresampled($new_img, $image, 0, 0, 0, 0, $resize_width, $resize_height, $orig_width, $orig_height);
        } else {
            //FIX WIDTH AND HEIGHT
            $maxWidth  = 1080;
            $maxHeight = 1080;
            // Calculate RATIO OF MAXIMUM SIZE AND ORIGINAL
            $widthRatio  = $maxWidth / $orig_width;
            $heightRatio = $maxHeight / $orig_height;
            // RATIO USED FOR CALCULATEING NEW IMAGE.
            $ratio = min($widthRatio, $heightRatio);
            // Calculate new image CALCULATE NEW IMAGE.
            $newWidth  = (int) $orig_width  * $ratio;
            $newHeight = (int) $orig_height * $ratio;
            // CREATE NEW IMAGE
            $new_img = imagecreatetruecolor($newWidth, $newHeight);
            $image   = $fileName;
            imagecopyresampled($new_img, $image, 0, 0, 0, 0, $newWidth, $newHeight, $orig_width, $orig_height);
        }
        return $new_img;
    }

    public static function ftp_mksubdirs($ftpCon, $ftpBaseDir, $ftPath)
    {
        @ftp_chdir($ftpCon, $ftpBaseDir);
        $parts = explode('/', $ftPath);
        foreach ($parts as $part) {
            if (!@ftp_chdir($ftpCon, $part)) {
                ftp_mkdir($ftpCon, $part);
                ftp_chdir($ftpCon, $part);
                //ftp_chmod($ftpcon, 0777, $part);
            }
        }
    }

    public static function DeleteFtp($ftpServer, $fileDelete)
    {
        $ftp_del = null;
        if (!empty($fileDelete)) {
            //setup of connection
            $ftp_conn = ftp_connect($ftpServer->host, $ftpServer->port) or die("could not connect to " . $ftpServer->host . ":" . $ftpServer->port);
            $login    = ftp_login($ftp_conn, $ftpServer->username, $ftpServer->password);
            if (!empty($fileDelete)) { //SINGLE FILE
                $ftp_del = @ftp_delete($ftp_conn, $fileDelete);
            }
            ftp_close($ftp_conn);
        }
        return $ftp_del;
    }

    public static function GetIpAddress()
    {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }

    public static function GetIdApi($cardType, $prefix, $id)
    {
        $num = 13 - (strlen($cardType) + strlen($prefix));
        $newId = str_pad($id, $num, '0', STR_PAD_LEFT);
        return $cardType . $prefix . $newId;
    }

    // ฟังก์ชั่นจัดเรียงข้อมูลจากคีย์ เรียงจากน้อยไปมาก
    public static function sortArrDataByKey($arrData)
    {
        $arrSortData = array();
        foreach ($arrData as $key => $row) {
            $arrSortData[$key] = $key;
        }
        array_multisort($arrSortData, SORT_ASC, $arrData);
        return $arrData;
    }

    // ฟังก์ชันแยกข้อความให้อยู่ในรูปแบบหลายบรรทัด
    // $txt = ข้อความ
    // $num = จำนวนความยาวของตัวอักษรที่ต้องการ
    // return array
    public static function separateStringMultiLine($txt, $num = 156)
    {
        $arrData = [];
        $string = trim(preg_replace('/\s\s+/', ' ', $txt)); // ลบช่องว่างก่อนแหละหลังข้อความทั้งหมด
        if (!empty($string)) {
            if ($num <= 0) {
                $arrData[] = $string;
            } else {
                $txtNum = strlen($string);
                if ($txtNum <= $num) {
                    $arrData[] = $string;
                } else {
                    if (strpos($string, 'โทร')) {
                        $arrFnAddr = explode(' ', $string);
                        $arrAddr2 = array_slice($arrFnAddr, -6);
                        $arrAddr1 = array_diff($arrFnAddr, $arrAddr2);
                        $txtAddr1 = implode(' ', $arrAddr1);
                        $txtAddr2 = implode(' ', $arrAddr2);

                        $txtAddr1Num = strlen($txtAddr1);
                        if ($txtAddr1Num > ($txtNum / 2)) {
                            $arrData[] = $txtAddr1;
                            $arrData[] = $txtAddr2;
                        } else {
                            $arrFnAddr = explode(' ', $string);
                            $arrAddr2 = array_slice($arrFnAddr, -5);
                            $arrAddr1 = array_diff($arrFnAddr, $arrAddr2);
                            $txtAddr1 = implode(' ', $arrAddr1);
                            $txtAddr2 = implode(' ', $arrAddr2);
                            $arrData[] = $txtAddr1;
                            $arrData[] = $txtAddr2;
                        }
                    } else {
                        $arrFnAddr = explode(' ', $string);
                        $arrAddr2 = array_slice($arrFnAddr, -4);
                        $arrAddr1 = array_diff($arrFnAddr, $arrAddr2);
                        $txtAddr1 = implode(' ', $arrAddr1);
                        $txtAddr2 = implode(' ', $arrAddr2);

                        $txtAddr1Num = strlen($txtAddr1);
                        if ($txtAddr1Num > ($txtNum / 2)) {
                            $arrData[] = $txtAddr1;
                            $arrData[] = $txtAddr2;
                        } else {
                            $arrFnAddr = explode(' ', $string);
                            $arrAddr2 = array_slice($arrFnAddr, -3);
                            $arrAddr1 = array_diff($arrFnAddr, $arrAddr2);
                            $txtAddr1 = implode(' ', $arrAddr1);
                            $txtAddr2 = implode(' ', $arrAddr2);
                            $arrData[] = $txtAddr1;
                            $arrData[] = $txtAddr2;
                        }
                    }
                }
            }
        }

        return $arrData;
    }

    // ฟังก์ชันคำนวณจำนวนบรรทัดจากข้อความ
    // $text : ข้อความ
    // $maxwidth : ความยาวที่ต้องการในแต่ละบรรทัด
    // return integer
    public static function getNbLineMultiCellForPDF($pdf, $maxwidth, $text)
    {
        $text = trim($text);
        if ($text === '')
            return 0;
        $space = $pdf->GetStringWidth(' ');
        $lines = explode("\n", $text);
        $text = '';
        $count = 0;
        foreach ($lines as $line) {
            $words = preg_split('/ +/', $line);
            $width = 0;

            foreach ($words as $word) {
                $wordwidth = $pdf->GetStringWidth($word);
                if ($wordwidth > $maxwidth) {
                    // Word is too long, we cut it
                    for ($i = 0; $i < strlen($word); $i++) {
                        $wordwidth = $pdf->GetStringWidth(substr($word, $i, 1));
                        if ($width + $wordwidth <= $maxwidth) {
                            $width += $wordwidth;
                            $text .= substr($word, $i, 1);
                        } else {
                            $width = $wordwidth;
                            $text = rtrim($text) . "\n" . substr($word, $i, 1);
                            $count++;
                        }
                    }
                } elseif ($width + $wordwidth <= $maxwidth) {
                    $width += $wordwidth + $space;
                    $text .= $word . ' ';
                } else {
                    $width = $wordwidth + $space;
                    $text = rtrim($text) . "\n" . $word . ' ';
                    $count++;
                }
            }

            $text = rtrim($text) . "\n";
            $count++;
        }
        $text = rtrim($text);
        return $count;
    }

    // แยกตัวเลขออกจากข้อความ
    // $text = ข้อความที่ต้องการแยก
    public static function getNumberOnly($text)
    {
        $res = null;
        if (!empty($text)) {
            preg_match_all('!\d+!', $text, $matches);
            foreach ($matches[0] as $key => $val) {
                $res .= $val;
            }
        }
        return $res;
    }

    // https://stackoverflow.com/questions/10313332/how-to-highlight-search-results
    // ติดป้ายสีสำหรับคำที่ต้องการ / คำที่ค้นหา
    public static function highlight_word( $content, $word, $color = 'yellow' ) {
        $replace = '<span style="background-color: ' . $color . ';">' . $word . '</span>'; // create replacement
        $content = str_replace( $word, $replace, $content ); // replace content
    
        return $content; // return highlighted data
    }

    // ติดป้ายสีสำหรับคำที่ต้องการ / คำที่ค้นหา
    public static function highlight_words( $content, $words ) {
        $color_index = 0; // index of color (assuming it's an array)
    
        // loop through words
        foreach( $words as $word ) {
            $content = self::highlight_word( $content, $word ); // highlight word
        }
    
        return $content; // return highlighted data
    }

    // หาวัน จากวันที่
    public static function getDayName($date) {
        $seven_day = array('อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัส', 'ศุกร์', 'เสาร์');
        $day_name = $seven_day[date('w', strtotime($date))];

        return $day_name;
    }
}
