<?php
$this->title = 'Introduction';
?>
<div class="site-index">

    <div>
        <article>
            <h3>Endpoints</h3>
            <p >The API is accessed by making HTTP requests to a specific version endpoint URL, in which GET, POST, PUT, PATCH, and DELETE methods dictate how your interact with the information available.</p>
            <figure class="highlight">
                <pre>
                    <code>http://43.240.112.146:8090/api/<?php //echo Yii::getAlias('@web'); ?></code>
                </pre>
            </figure>
        </article>

        <article>
            <h3>Headers</h3>
            <p>All requests must include parameters headers to authenticate.</p>
            <h4><em>Headers</em> parameters</h4>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr class="info">
                        <th>Data key</th>
                        <th>Data value</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><strong>app_code</strong></td>
                        <td><em>xxx</em></td>
                        <td>APP CODE</td>
                    </tr>
                </tbody>
            </table>
        </article>

        <article>
            <h3>Requests</h3>
            <p>Requests must be sent over HTTP with any payload formatted in JSON. All requests must include parameters headers to authenticate.</p>
            <h4><em>Required</em> parameters</h4>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr class="info">
                        <th>Data Name</th>
                        <th>Data Type</th>
                        <th>Data Require</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><strong>booking_id</strong></td>
                        <td><em>integer</em></td>
                        <td><em>No</em></td>
                        <td>Booking ID</td>
                    </tr>
                </tbody>
            </table>
        </article>

    <article>
        <h3>Responses</h3>
        <div>
            <h4>Format</h4>
            <p>Each response is a JSON object. The data requested is wrapped in the <span class="label label-primary">result</span> tag. If you have a response, it will always be within the <span class="label label-primary">result</span> field. We also include a <span class="label label-success">success</span> flag, an array of potential <span class="label label-danger">errors</span> and <span class="label label-warning">messages</span> in the response.</p>
            <p>An error object will contain an integer <span class="label label-default">code</span> field and a <span class="label label-warning">message</span></p>
        </div>
        
        
        <div class="bs-example"><h4 class="success">Success Response</h4></div>
        <figure class="highlight">
            <pre>
                <code>
                {
                    "success": true,
                    "errors": [],
                    "messages": '',
                }
                </code>
            </pre>
        </figure>

        <div class="bs-example"><h4 class="danger">Error Response</h4></div>
        <figure class="highlight">
            <pre>
                <code>
                {
                    "success": false,
                    "errors": {
                        "jcbook_id": [
                            "Jcbook ID ต้องไม่ว่างเปล่า"
                        ],
                        "receipt_type": [
                            "Receipt Type ต้องไม่ว่างเปล่า"
                        ]
                    },
                    "messages": 'Can not save to database!'
                }
                </code>
            </pre>
        </figure>
    </article>
    </div>
</div>
