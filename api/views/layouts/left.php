<?php 
use yii\helpers\Html;

use common\models\Jcbook;
use common\models\JcwalkinDetail;
?>
<aside class="main-sidebar">

    <section class="sidebar">
    <?php
        $menuItems = array();
        //FOR ALL
        $menuItems = [
            [
                'label' => 'Introduction',
                'url' => ['site/index'],
                'visible' => (Yii::$app->controller->action->id == 'index')?true:false,
                'icon' => 'home'.Yii::$app->controller->action->id,
            ],
            [
                'label' => 'Auth Transection',
                'url' => '#',
                'visible' => (Yii::$app->controller->action->id == 'auth-transection')?true:false,
                'icon' => 'file-code-o',
                'items' => [
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Login',
                        'url' => ['site/auth-transection', '#'=>'login'],
                    ]
                ],
            ],
            [
                'label' => 'Master Manage',
                'url' => '#',
                'icon' => 'file-code-o',
                'visible' => (Yii::$app->controller->action->id == 'master-manage')?true:false,
                'items' => [
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Car Brand',
                        'url' => ['site/master-manage', '#'=>'get-car-brand'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Car Model',
                        'url' => ['site/master-manage', '#'=>'get-car-model'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Car Series',
                        'url' => ['site/master-manage', '#'=>'get-car-series'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Car Color',
                        'url' => ['site/master-manage', '#'=>'get-car-color'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Car Model Type',
                        'url' => ['site/master-manage', '#'=>'get-car-model-type'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Insurance Name',
                        'url' => ['site/master-manage', '#'=>'get-insurance-comp'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Finance Name',
                        'url' => ['site/master-manage', '#'=>'get-finance-comp'],
                    ],
                ],
            ],
            [
                'label' => 'Booking Transection',
                'url' => '#',
                'icon' => 'file-code-o',
                'visible' => (Yii::$app->controller->action->id == 'booking-transection')?true:false,
                'items' => [
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Booking Receipt',
                        'url' => ['site/booking-transection', '#'=>'booking-receipt'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Confirm PO',
                        'url' => ['site/booking-transection', '#'=>'booking-confirm-po'],
                    ],
                ],
            ],
            [
                'label' => 'Repaircar Transection',
                'url' => '#',
                'icon' => 'file-code-o',
                'visible' => (Yii::$app->controller->action->id == 'bp-repaircar')?true:false,
                'items' => [
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Repaircar List Status Process Data',
                        'url' => ['site/bp-repaircar', '#'=>'get-repaircar-list-status-process-data'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Repaircar List Status Process',
                        'url' => ['site/bp-repaircar', '#'=>'get-repaircar-list-status-process'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Repaircar List Floor',
                        'url' => ['site/bp-repaircar', '#'=>'get-repaircar-list-floor'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get List Province',
                        'url' => ['site/bp-repaircar', '#'=>'get-list-province'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get List User',
                        'url' => ['site/bp-repaircar', '#'=>'get-list-user'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Repaircar List On Process',
                        'url' => ['site/bp-repaircar', '#'=>'get-repaircar-list-on-process'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Repaircar List Wait Process',
                        'url' => ['site/bp-repaircar', '#'=>'get-repaircar-list-wait-process'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Repaircar List Send Car',
                        'url' => ['site/bp-repaircar', '#'=>'get-repaircar-list-send-car'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Repaircar Information',
                        'url' => ['site/bp-repaircar', '#'=>'get-repaircar-information'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Repaircar Files',
                        'url' => ['site/bp-repaircar', '#'=>'get-repaircar-file'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Save Repaircar Information',
                        'url' => ['site/bp-repaircar', '#'=>'save-repaircar-information'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Save Repaircar Process',
                        'url' => ['site/bp-repaircar', '#'=>'save-repaircar-process'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Save Repaircar Assign Floor',
                        'url' => ['site/bp-repaircar', '#'=>'save-repaircar-assign-floor'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Save Repaircar Assign User',
                        'url' => ['site/bp-repaircar', '#'=>'save-repaircar-assign-user'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Save Repaircar Select Process',
                        'url' => ['site/bp-repaircar', '#'=>'save-repaircar-select-process'],
                    ],
                ],
            ],
            [
                'label' => 'Traffic Cone Transection',
                'url' => '#',
                'icon' => 'file-code-o',
                'visible' => (Yii::$app->controller->action->id == 'traffic-cone')?true:false,
                'items' => [
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get List Color Cone',
                        'url' => ['site/traffic-cone', '#'=>'get-list-color-cone'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'Get Jcrepair On Process',
                        'url' => ['site/traffic-cone', '#'=>'get-jcrepair-on-process'],
                    ],
                    [
                        'icon' => 'arrow-right',
                        'label' => 'SAVE TRAFFIC CONE',
                        'url' => ['site/traffic-cone', '#'=>'save-traffic-cone'],
                    ],
                ],
            ],
        ];
        ?>
        
        <?php echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $menuItems,
                'encodeLabels' => false,
            ]
        ); ?>

    </section>

</aside>
