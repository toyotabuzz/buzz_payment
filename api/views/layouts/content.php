<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">
	<div class="content-header">
		<?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } else { ?>
            <h1 class="page-header">
                <?php
                if ($this->title !== null) {
                    echo \yii\helpers\Html::encode($this->title);
                } else {
                    echo \yii\helpers\Inflector::camel2words(
                        \yii\helpers\Inflector::id2camel($this->context->module->id)
                    );
                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                } ?>
            </h1>
        <?php } ?>

		<?php echo Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
	</div>

	<div class="content body">
	    <section class="introduction">
	        <?php echo Alert::widget() ?>
	        <?php echo $content ?>
	    </section>		
	</div>
</div>

<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date('Y') ?> TOYOTA BUZZ.</strong> All rights reserved.
</footer>

<div class='control-sidebar-bg'></div>