<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class PaymentAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'components/fontawesome/css/font-awesome.min.css',
        'components/sweetalert2/sweetalert2.min.css',
        'css/site.css',
        'css/payment.css',
        // '//fonts.googleapis.com/css?family=Kanit|Prompt'
    ];
    public $js = [
        'components/sweetalert2/sweetalert2.min.js',
        'js/iframeResizer.contentWindow.min.js',
        'js/loadingoverlay.min.js',
        'js/loadingoverlay_progress.min.js',  
        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}
