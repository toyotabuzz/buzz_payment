<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use yii\data\ActiveDataProvider;
use Codeception\Test\Unit;
use yii\authclient\InvalidResponseException;
use yii\console\ErrorHandler;
use yii\httpclient\Response;

use app\components\Apiservice;

class PaymentController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    //No Login
                    [
                        'actions' => [
                            'index', 'create', 'get-form-payment', 'verify-send-sms', 'save-verify-send-sms', 'verify-payment', 'get-data-formall', 'check-data-redirect', 'check-data-redirect-qr', 'load-form', 'get-order-id', 'get-button-qr-code',
                        ],
                        'allow' => true,
                        // 'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    //ปิดการใช้งานตรวจสอบ csrf 
    // public function beforeAction($action)
    // {
    //     $this->enableCsrfValidation = false;
    //     return parent::beforeAction($action);
    // }

    //เข้ารหัสถอดรหัส code_check
    public function encodeCodecheck($txtCode, $flag)
    {
        $arrTranscode = [];
        $txtCodecheck = '';
        $codeCheck = '';
        $message = '';
        $success = false;
        $secretKey = 'buzz^^';
        if (!empty($txtCode) && !empty($flag)) {
            //เข้ารหัสตอนรับเพื่อส่ง SMS (f002115515sfd)
            if ($flag == 'encode') {
                $arrExplode = explode("|", $txtCode);
                if (!empty($arrExplode)) {
                    $method = empty($arrExplode[0]) ? null : $arrExplode[0];
                    $ref_id = empty($arrExplode[1]) ? null : $arrExplode[1];
                    $create_at = empty($arrExplode[2]) ? null : $arrExplode[2];
                    if (!empty($method) && !empty($ref_id) && !empty($create_at)) {
                        $txtCode = $method . '|' . $ref_id . '|' . $create_at;
                        $codeCheck = base64_encode(Yii::$app->security->encryptByKey($txtCode, $secretKey));
                        $success = true;
                    } else {
                        $message = '!ไม่พบสัญลักษณ์ | ในการเข้ารหัส';
                    }
                } else {
                    $message = '!ไม่สามารถเข้ารหัสได้';
                }
                //แปลงรหัสให้เป็นค่า method, ref_id โดยมี | คั่นต้อง explode
            } else if ($flag == 'decode') {
                $keyConvert = Yii::$app->security->decryptByKey(base64_decode($txtCode), $secretKey);
                if (!empty($keyConvert)) {
                    $arrExplode = explode("|", $keyConvert);
                    if (!empty($arrExplode[0]) && $arrExplode[1] && $arrExplode[2]) {
                        $method = empty($arrExplode[0]) ? null : $arrExplode[0];
                        $ref_id = empty($arrExplode[1]) ? null : $arrExplode[1];
                        $created_at = empty($arrExplode[1]) ? null : $arrExplode[1];
                        $success = true;
                    } else {
                        $message = '!ไม่พบสัญลักษณ์ | ในการถอดรหัส';
                    }
                    // JuPayment::find()->where(['medhod' => $arrExplode[0]])->andWhere(['ref_id' => $arrExplode[0]])->one();
                } else {
                    $message = '!ไม่สามารถถอดรหัสได้';
                }
            }
        } else {
            $message = '!Parameter (flag) ไม่มีข้อมูลส่งมา';
        }
        return  $arrTranscode = [
            'success' =>  empty($success) ? null : $success,
            'code_check' => empty($codeCheck) ? null : $codeCheck,
            'method_name' => empty($method) ? null : $method,
            'ref_id' => empty($ref_id) ? null : $ref_id,
            'created_at' => empty($create_at) ? null : $create_at,
            'message' => empty($message) ? null : $message,
        ];
    }
    //ตอนกดปุ่มมาจ่ายเงิน
    //ส่ง SMS
    //http://localhost:8082/buzz_payment/web/payment/verify-send-sms?method=booking&ref_id=159&amount=15000&customer_name=วรากร ประดิษฐกุล&customer_tel=0877924834&merchant_id=001265540001&merchant_name=บริษัท โตโยต้าบัสส์ จํากัด&text_sms=กดเข้าลิ้งค์นี้เพื่อชำระเงินค่าจองรถยนต์.               
    //https://payment.toyotabuzz.com/paymentgateway/web/payment/verify-send-sms?method=booking&ref_id=159&amount=15000&customer_name=วรากร ประดิษฐกุล&customer_tel=0877924834&merchant_id=001265540001&merchant_name=บริษัท โตโยต้าบัสส์ จํากัด&text_sms=กดเข้าลิ้งค์นี้เพื่อชำระเงินค่าจองรถยนต์.               
    public function actionVerifySendSms()
    {
        $this->layout = 'blank';
        $requestUrl = Yii::$app->request->get();
        // $urlPrefix = $requestUrl['text_sms'] . ' https://buzzpower.in.th/buzz_power/frontend/web/payment/payment/verify-payment/';
        $setUrlList = empty($requestUrl['text_sms']) ? null : $requestUrl['text_sms'];
        $setUrl = Yii::$app->params['urlVerifyPayment'];
        $txtCode = '';
        $res = [];
        $res['status'] = false;
        $res['message'] = 'เตือน! เกิดข้อผิดพลาด กรุณาแจ้งผู้ดูแลระบบ';
        // echo '<pre>'; print_r($requestUrl);
        if (!empty($requestUrl)) {
            $res['check_payment'] = $this->checkDataSendSms($requestUrl);
            // echo '<pre>'; print_r($res['check_payment']); exit;
            //ถ้ายังไม่เคยส่ง SMS มาก่อน
            if (!empty($res['check_payment']) && !empty($res['check_payment']['success'])) {
                $res['add_code']['code_check'] = $this->addCodeCheck($requestUrl['method'], $requestUrl['ref_id']); //ชุด 1 รวมตัวแปร method|ref_id|date_current
                $res['add_code']['code_recheck'] = $this->addCodeCheck($requestUrl['merchant_id'], $requestUrl['ref_id']); //ชุด 2 รวมตัวแปร merchant_id|ref_id|date_current
                if (!empty($res['add_code']['code_check']['success']) && $res['add_code']['code_recheck']['success']) {
                    //สร้าง URL
                    $setUrl .= 'codeCheck=' . $res['add_code']['code_check']['add_encode']['code_check'];
                    $setUrl .= '&codeRecheck=' . $res['add_code']['code_recheck']['add_encode']['code_check'];
                    //set ให้ตัวแปร Request เพื่อแสดง | บันทึก
                    $requestUrl['code_check'] = $res['add_code']['code_check']['add_encode']['code_check'];
                    $requestUrl['code_recheck'] = $res['add_code']['code_recheck']['add_encode']['code_check'];
                }
                //เคยส่ง SMS แล้ว
            } else {
                $res['success'] = false;
                $res['message'] = $res['check_payment']['message'];
                $requestUrl['code_check'] = $res['check_payment']['results']['code_check'];
                $requestUrl['code_recheck'] = $res['check_payment']['results']['code_recheck'];
                $requestUrl['text_sms'] = empty($res['check_payment']['results']['text_sms']) ? $requestUrl['text_sms'] : $res['check_payment']['results']['text_sms'];
            }
        }
        return $this->render('_form-verify-send-sms', [
            'resRequest' => $requestUrl,
            'resPayment' => empty($res['check_payment']) ? null : $res['check_payment'],
            'resFullUrl' => empty($setUrl) ? null : $setUrl,
            'resShortUrl' => empty($res['bitly_url']) ? null : $res['bitly_url'],
        ]);
    }

    public  function actionSaveVerifySendSms()
    {
        $request = Yii::$app->request->get();
        $setUrl = '';
        // echo '<pre>';
        // print_r($request);
        $res = [];
        $res['title']  = '';
        $res['message'] = 'กรุณากดปุ่มส่ง SMS อีกครั้ง';
        $res['success'] = false;
        if (!empty($request)) {
            //แปลง URL
            $res['bitly_convert'] = $this->bitlyShorturl($request['link_before_convert']);
            // echo '<pre>';
            // print_r($res['bitly_convert']);
            // exit;
            if (!empty($res['bitly_convert']['success']) && !empty($res['bitly_convert']['link'])) {
                //เอาข้อความที่ส่งมาเพิ่มไปในข้อความ 
                $setUrl = empty($request['text_sms']) ? $res['bitly_convert']['link'] : $request['text_sms'] . ' ' . $res['bitly_convert']['link'];
                //ส่ง SMS
                $res['send_sms'] = $this->sendSMS($request['customer_tel'], $setUrl);
                if ($res['send_sms']['success']) {
                    //บันทึก Paymemt (NodeJs)
                    $request['text_sms'] = empty($setUrl) ? null : $setUrl;
                    $request['created_by'] = 'system';
                    $request['updated_by'] = 'system';
                    $request['bitly_url'] = empty($res['bitly_convert']['link']) ? null : $res['bitly_convert']['link'];
                    $res['save_data'] = Apiservice::sendApiNodeService('payment/save-jupayment-send-sms?' . http_build_query($request));
                    if ($res['save_data']['success']) {
                        $res['success'] = $res['save_data']['success'];
                        $res['title'] = $res['save_data']['title'];
                        $res['message'] = $res['save_data']['message'];
                    } else {
                        $res['message'] = $res['save_data']['message'];
                    }
                    // echo '<pre>';
                    // print_r($res['save_data']);
                    // exit;
                } else {
                    $res['message'] = $res['send_sms']['message'];
                }
            } else {
                $res['message'] = $res['bitly_convert']['message'];
            }
        }
        return json_encode($res);
    }
    //ตรวจสอบสอบลิ้งที่คลิดจากมือถือ
    //http://localhost:8082/buzz_payment/web/payment/verify-payment?codeCheck=pFOr+yrFtlUmmKm5UVDHhjgxZDY2NmFiMjNhMDRiYzdjOTY3NTIzYTE4M2YzMzZhODFjOWY4ZmY5MDg2NjJlODQxMjM5YzMwMmYwZDY2ZWa11Ws0NsKaDBe1K5gOUxufAGg0vigtBQ4/3Eb/sOtmBF8Kbhnvv66XkG2sMgKjlww=&codeRecheck=96pw6wlfF9YaAbzTLgqWfjM1MTkxZDY4MmY0Mjk5MmVmNWIyZmI0ZDNmYjMwNzlhNmU4YzhmYTU2NjY5MjQyOTdlZTMyOWI0ZTE2YTFiMDBYPPb4iamEw1bGNVgOEJbsm54jqruo3bvi+Rw693K0l7ygL9PgN91ckW13JWLQtb0v5j+kPcRnZUT5l81Mb85W
    //https://payment.toyotabuzz.com/paymentgateway/web/payment/verify-payment?codeCheck=pFOr+yrFtlUmmKm5UVDHhjgxZDY2NmFiMjNhMDRiYzdjOTY3NTIzYTE4M2YzMzZhODFjOWY4ZmY5MDg2NjJlODQxMjM5YzMwMmYwZDY2ZWa11Ws0NsKaDBe1K5gOUxufAGg0vigtBQ4/3Eb/sOtmBF8Kbhnvv66XkG2sMgKjlww=&codeRecheck=96pw6wlfF9YaAbzTLgqWfjM1MTkxZDY4MmY0Mjk5MmVmNWIyZmI0ZDNmYjMwNzlhNmU4YzhmYTU2NjY5MjQyOTdlZTMyOWI0ZTE2YTFiMDBYPPb4iamEw1bGNVgOEJbsm54jqruo3bvi+Rw693K0l7ygL9PgN91ckW13JWLQtb0v5j+kPcRnZUT5l81Mb85W
    public function actionVerifyPayment()
    {
        $this->layout = 'blank';
        $requestUrl = Yii::$app->request->get();
        $resultNode = [];
        if (!empty($requestUrl['codeCheck']) && !empty($requestUrl['codeRecheck'])) {
            //ตรวจสอบการจ่าย (Node)
            $resultNode = $this->checkDataPayment($requestUrl);
            // echo '<pre>';
            // print_r($resultNode);
            // exit;
            if (empty($resultNode['success'])) { //ถ้ามี error
                return $this->render('_form-verify-payment', [
                    'resultError' => $resultNode['message'],
                    'resultIcon' => $resultNode['icons'],
                    'resultData' => $resultNode['results'],
                ]);
            } else { //ไม่ error ไปที่หน้าเลือกประเภทการชำระ
                return $this->render('_form-select-type', [
                    'resultIcon' => $resultNode['icons'],
                    'resultData' => $resultNode['results'],
                ]);
            }
        }
    }
    //บันทึกข้อมูลตอนกดเลือกรูปแบบการจ่าย
    public function saveDataPayType()
    {
        $request = Yii::$app->request->get();
        // echo '<pre>';
        // print_r($request);
        // exit;
        $res   = [];
        $res['title']  = '';
        $res['message'] = '';
        $res['success'] = false;
        if (!empty($request)) {
        }
        return json_encode($res['save_data']);
    }
    //เอา ตัวแปรมาเข้ารหัส
    public function addCodeCheck($txt1, $txt2)
    {
        $res['code'] = '';
        $res['message'] = 'ข้อมูลที่ส่งมาไม่ครบ [method, ref_id, merchant_id]';
        $res['success'] = false;
        if (!empty($txt1) && !empty($txt2)) {
            $res['success'] = true;
            $res['full_code'] = rtrim($txt1) . '|' . rtrim($txt2) . '|' . date('Y-m-d H:i:s');
            $res['message'] = 'สร้าง CodeCheck, CodeReCheck เรียบร้อย';
            //ส่งไปเข้ารหัส
            if ($res['full_code']) {
                $res['add_encode'] = $this->encodeCodecheck($res['full_code'], 'encode');
                // echo '<pre>'; print_r($res['add_encode']);exit;
                if ($res['add_encode']['success']) {
                    $res['success'] = true;
                }
            }
        }
        return $res;
    }
    //ตรวจสอบว่า method, ref_id นี้ส่ง SMS ยัง 
    public function checkDataSendSms($requestUrl)
    {
        $res['title'] = 'กรุณากดเข้า LINK ใหม่อีกครั้ง';
        $res['icon'] = '';
        $res['message'] = '';
        $res['success'] = false;
        $resultNode = Apiservice::sendApiNodeService('payment/check-data-send-sms?' . http_build_query($requestUrl));
        return $resultNode;
    }
    //ตรวจสอบ code_check, code_recheck จากลิ้งค์มือถือ
    public function checkDataPayment($requestUrl)
    {
        $resultNode = [];
        if (!empty($requestUrl)) {
            if (empty($requestUrl['codeCheck']) || empty($requestUrl['codeRecheck'])) {
                Yii::$app->session->setFlash('warning', 'โปรดตรวจสอบลิ้งค์ อีกครั้ง');
            } else {
                //เปลี่ยนช่องว่าง เป็น (+) ก่อนส่งไป NodeJs
                $requestUrl['code_check'] = $requestUrl['codeCheck'];
                $requestUrl['code_recheck'] = $requestUrl['codeRecheck'];
                $resultNode = Apiservice::sendApiNodeService('payment/check-data-payment?' . http_build_query($requestUrl));
            }
        }
        return $resultNode;
    }

    public function actionGetDataFormall()
    {
        $request = Yii::$app->request->get();
        $res = [];
        $res['title']   = '';
        $res['message'] = 'กรุณากดเข้าลิงค์ใหม่';
        $res['link'] = '';
        $res['success'] = false;
        if (!empty($request['formId'])) {
            //link แต่ละฟอร์ม
            if ($request['formId'] == 1) {
                $txtLink = '_form-pay-credit';
                $sourceType = 'card';
            } else if ($request['formId'] == 2) {
                $txtLink = '_form-pay-qrcode';
                $sourceType = 'qr';
            } else {
                $txtLink = '_form-pay-alipay';
                $sourceType = 'card';
            }
            $resultNode = Apiservice::sendApiNodeService('payment/get-data-formall?' . http_build_query($request));
            // echo '<pre>';
            // print_r($resultNode);
            // exit;
            if (!empty($resultNode['success'])) {
                $res['message'] = $resultNode['message'];
                $res['link'] = $txtLink;
                $res['sourceType'] = $sourceType;
                $res['success'] = true;
            } else {
                $res['message'] = $resultNode['message'];
            }
        }
        return json_encode($res);
    }


    // public function actionGetFormPayment()
    // {
    //     $request = Yii::$app->request->get();
    //     $res = [];
    //     $res['title']   = '';
    //     $res['message'] = 'กรุณาเลือกรูปแบบการชำระอีกครั้ง';
    //     $res['success'] = false;
    //     $request['user_id'] = 'system';
    //     $resultNode = Apiservice::sendApiNodeService('payment/save-data-paytype?' . http_build_query($request));
    //     // echo '<pre>';
    //     // print_r($resultNode);
    //     // exit;
    //     if (!empty($resultNode) && !empty($request['linkForm'])) {
    //         $link = $request['linkForm'];
    //         $res['title']   = 'ตรวจสอบเรียบร้อย';
    //         $res['message'] = 'เลือกรูปแบบการชำระเรียบร้อย';
    //         $res['success'] = true;
    //     }
    //     $res['html'] = $this->renderAjax($link, [
    //         // 'modelBook' => $modelBook,
    //         // 'arrItem' => empty($arrItem) ? null : $arrItem,
    //     ]);
    //     return json_encode($res);
    // }

    //เข้าหน้าฟอร์มจ่ายเงินที่เลือก
    public function actionGetFormPayment()
    {
        $this->layout = 'blank';
        $dataRequest = Yii::$app->request->get();
        // echo '<pre>';
        // print_r($dataRequest);
        // exit;
        if (!empty($dataRequest['link']) && !empty($dataRequest['payment_id'])) {
            //ดึงข้อมูลด้วย jupayment_id
            $resultNode = Apiservice::sendApiNodeService('payment/get-data-payment?' . http_build_query($dataRequest));
            // echo '<pre>';
            // print_r($resultNode);
            // exit;
            if ($resultNode['success']) {
                return $this->render($dataRequest['link'], [
                    'sourceType' => $dataRequest['source_type'],
                    'resultData' => $resultNode['results'],
                ]);
            } else {
                Yii::$app->getSession()->setFlash('errorapi', [
                    'body' => '<i class="glyphicon glyphicon-alert"></i> <b>แจ้งเตือน</b><br><hr>' . $resultNode['message'],
                    'options' => ['class' => 'alert-danger', 'id' => 'box-payment-alert']
                ]);
            }
        } else {
        }
    }
    //ตรวจสอบข้อมูลหลังกดบันทึก
    public function actionCheckDataRedirect()
    {
        $dataPost = Yii::$app->request->post();
        $dataForm = array();
        if (!empty($dataPost['token'])) {
            $token = $dataPost['token'];
            $dataForm = [
                "amount"          => $dataPost['amount'],
                "currency"        => "THB",
                "description"     => $dataPost['textSms'],
                "source_type"     => $dataPost['sourceType'],
                "mode"            => "token",
                "token"           => $token,
                "reference_order" => "11251511"
            ];
        }

        $apiResult = Yii::$app->Apiservice->sendDataKbank($dataForm);
        // echo '<pre>';
        // print_r($apiResult);
        // exit;

        if (!empty($apiResult['status'])) {
            // $res['msg']  = 'ระบบดำเนินการส่ง SMS เรียบร้อยแล้ว ข้อความว่า <br/><br/><p>" ' . $contentSMS . ' "</p>';
            return $this->redirect($apiResult['redirect_url']);
        } else {
            echo '<pre>';
            print_r($apiResult);
            exit;
        }
    }
    //ตรวจสอบข้อมูลหลังกดบันทึก
    public function actionCheckDataRedirectQr()
    {
    }
    //API order_id 
    public function actionGetOrderId()
    {
        $res = [];
        $res['success'] = false;
        $res['message'] = null;
        $dataForm = [];
        $dataPost = Yii::$app->request->post();
        // echo '<pre>';
        // print_r($dataPost);
        // exit;
        if (!empty($dataPost)) {
            $dataForm = [
                "amount"          => $dataPost['amount'],
                "currency"        => "THB",
                "description"     => $dataPost['text_sms'],
                "source_type"     => $dataPost['source_type'],
                "reference_order" => $dataPost['order_id'],
                // "customer"        => $dataPost['customer_name'],
                "metadata" => [[
                    "item"   => "paper",
                    "qty"    => "6",
                    "amount" => $dataPost['amount']
                ]]
            ];
        } else {
        }

        $apiResult = Yii::$app->Apiservice->sendGetOrderID($dataForm);
        // echo '<pre>';
        // print_r($apiResult);
        // exit;
        if (!empty($apiResult)) {
            if ($apiResult['object'] != 'error') {
                $res['order_id'] = $apiResult['id'];
                $res['amount'] = $apiResult['amount'];
                $res['success'] = true;
            } else {
                $res['message'] = $apiResult['message'];
            }
        }
        return json_encode($res);
    }
    //
    public function actionGetButtonQrCode()
    {
        $this->layout = 'blank';
        $res = [];
        $dataGet = Yii::$app->request->get();
        if (!empty($dataGet['amount']) && !empty($dataGet['order_id'])) {
            return $this->render("_ajax-get-qrcode", [
                'amount' => empty($dataGet['amount']) ? 0.00 : $dataGet['amount'],
                'order_id' => empty($dataGet['order_id']) ? null : $dataGet['order_id']
            ]);
        }
    }
    //รับข้อมูลจากธนาคารกรณีใส่เอง OTP 
    public function actionPaymentResult()
    {
        $dataPost = Yii::$app->request->post();
        $dataGet = Yii::$app->request->get();
        $dataResponse = empty($dataPost) ? $dataGet : $dataPost;

        echo '<pre>';
        print_r($dataResponse);
        exit;
    }

    //แปลง URL Bitly
    public function bitlyShorturl($fullUrl)
    {
        $res = [];
        $res['success'] = false;
        $res['message'] = 'เตือน! เกิดข้อผิดพลาดกรุณาแจ้งผู้ดูแลระบบ';
        $res['error'] = '';
        $res['link'] = '';
        $txtMassage = '';
        if (!empty($fullUrl)) {
            $urlData = [
                'long_url' => $fullUrl,
            ];
            $result = Apiservice::sendApiNodeService('bitly/convert-short-link?' . http_build_query($urlData));
            $resultDecode = json_decode($result);
            // echo '<pre>';
            // print_r($resultDecode);
            // exit;
            if (!empty($resultDecode->link)) {
                $res['success'] = true;
                $res['message'] = 'ระบบดำเนินแปลง URL เรียบร้อยแล้ว : " ' . $resultDecode->link . ' "</pre>';
                $res['link'] = $resultDecode->link;
            } else {
                $res['message'] = 'ระบบไม่สามารถแปลง URL ได้ โปรดตรวจสอบ URL';
                if (!empty($resultDecode->message)) {
                    $res['message'] = $resultDecode->message;
                } else if (!empty($resultDecode->error)) {
                    $errNum = 0;
                    foreach ($resultDecode->resultDecode as $key => $value) {
                        $res['message'] .= "<br>" . ++$errNum . ". " . $value;
                    }
                }
            }
        }
        return $res;
    }
    //ส่ง SMS
    public function sendSMS($mobileNo, $txtMassage)
    {
        $res  = [];
        $res['success'] = false;
        $res['title'] = '';
        $res['message'] = 'เตือน! เกิดข้อผิดพลาดไม่สามารถส่ง SMS ได้';
        if (!empty($mobileNo) && !empty($txtMassage)) {
            $smsData = [
                'to'   => Yii::$app->Helpers->formatMobileNumberToSMS($mobileNo),
                'text' => $txtMassage,
            ];
            $result = Apiservice::sendApiNodeService('sms/send-sms?' . http_build_query($smsData));
            $resultDecode = json_decode($result);
            if (!empty($resultDecode->details[0]->status->name)) {
                if ($resultDecode->details[0]->status->name == 'PENDING') {
                    $res['success'] = true;
                } else {
                    $res['message'] = $resultDecode->details[0]->status->description;
                }
            }
            // $res['message'] = 'ระบบดำเนินการส่ง SMS เรียบร้อยแล้ว ข้อความว่า <br/><br/><p>" ' . $txtMassage . ' "</p>';
        }
        return $res;
    }

    public function actionUpdate($id)
    {
        $model = $this->findModelPayment($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModelPayment($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModelPayment($id)
    {
        if (($model = JuPayment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
