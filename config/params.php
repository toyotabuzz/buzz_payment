<?php

return [
    //รหัส Api ants ส่ง SMS
    'antsUsername' => 'buzzit',
    'antsPassword' => 'Buzz1234',

    // SET NODE API 
    // 'apiNodeBaseUrl' => 'http://localhost:3000/api',   //Server Local
    // 'apiNodeBaseUrl' => 'http://43.240.112.146:8090/api', //Server Test
    'apiNodeBaseUrl' => 'http://10.22.192.116:3000/api', //Server Test
    'apiNodeAppCode' => 'BUZZ_POWER',

    //Token API Url
    'shortUrl'      => 'https://api-ssl.bitly.com/v4/shorten',
    'shortUrlToken' => 'bc00eb13d682ccd76aaca7fb4c6364a22b0be367',
    //LINK ชื่อฟังก์ชั่นที่ส่งให้ลูกค้าเข้ามาจ่ายเงิน
    // 'urlVerifyPayment' => 'http://localhost:8082/buzz_payment/web/payment/verify-payment?'  //Server Local
    'urlVerifyPayment' => 'https://payment.t-buzzpower.com/payment/verify-payment?',   //Server Test
    'paymentMerchant'  => [
        'sureInsure' => [
            'publicKey' => 'pkey_test_20747Cw4j26bAkAm1m1Mw3z12KikquJWaziyb',
            'secretKey' => 'skey_test_20747CzCW6PkxX8NXgWL7CzMG94ubJCPSuE8y',
        ],
        'toyotaBuzz' => [
            'publicKey' => 'pkey_test_20755wl0OSyXBf91qbIKevDp0fipoWOBpDWP1',
            'secretKey' => 'skey_test_20755OBy3UJO0kknt7EopiXTUOF9C94gQXDCN',
        ]
    ],
    'urlChargeCard'   => 'https://dev-kpaymentgateway-services.kasikornbank.com/card/v2/charge',
    'urlChargeQrcode' => 'https://dev-kpaymentgateway-services.kasikornbank.com/qr/v2/qr',
    
    'pathImageBase'       => '../img/payment',
    'pathImageSureinsure' => '../img/sure-insure',
    'pathImageToyotabuzz' => '../img/toyotabuzz',
];


// $param['paymentMerchant']['sureInsure']['publickey']