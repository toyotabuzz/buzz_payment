<?php

use yii\helpers\Url;
?>
<form method="POST" action="<?php echo Url::to(['payment/check-data-redirect-qr']); ?>">
    <script src="https://dev-kpaymentgateway.kasikornbank.com/ui/v2/kpayment.min.js" id="div-data" data-apikey="<?php echo Yii::$app->params['publickey']; ?>" data-amount="<?= $amount; ?>" data-lang="TH" data-payment-methods="qr" data-order-id="<?= $order_id; ?>">
    </script>
    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>" />
</form>
<script>
    $(function() {
        $("button").trigger("click");
    });
</script>