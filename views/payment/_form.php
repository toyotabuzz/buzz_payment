<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $modelPayment common\modelPayments\JuPayment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ju-payment-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL, //TYPE_HORIZONTAL
        'id' => 'form-set-payment',
        //'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        //SIZE_TINY = 'xs',SIZE_SMALL = 'sm',SIZE_MEDIUM = 'md',SIZE_LARGE = 'lg'
    ]); ?>
    <div class="row justify-content-md-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="box <?php echo ($modelPayment->isNewRecord) ? 'box-success' : 'box-warning' ?>">
                <div class="box-header with-border">
                    <i class="fa <?php echo ($modelPayment->isNewRecord) ? 'fa-plus-square' : 'fa-edit' ?>"></i>
                </div>
                <div class="box-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3">
                                <?php echo $form->field($modelPayment, 'method')->textInput(['maxlength' => true, 'value' => 'booking']) ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $form->field($modelPayment, 'ref_id')->textInput() ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $form->field($modelPayment, 'amount')->textInput(['value' => '1500']) ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $form->field($modelPayment, 'customer_name')->textInput(['maxlength' => true, 'value' => 'ดวงใจ สบายดี']) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <?php echo $form->field($modelPayment, 'merchant_id')->textInput(['maxlength' => true, 'value' => '111111111']) ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $form->field($modelPayment, 'merchant_name')->textInput(['maxlength' => true, 'value' => 'TOYOTABUZZ']) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer text-center">
                        <div class="col-md-12">
                            <?php echo Html::button('เลือกการชำระ', ['class' => 'btn-primary btn-lg btn-block', 'onclick' => 'choosePaytype();']); ?>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<script>
    function choosePaytype() {
        LoadingShow();
        $.ajax({
            type: 'post',
            url: baseUrl + '/payment/payment/get-form-paytype',
            data: $('form#form-set-payment').serializeArray(),
            dataType: 'json',
            success: function(res) {
                // console.log(res);
                if (!res.status) {
                    Swal.fire({
                        icon: 'warning',
                        title: 'ระบบพบข้อผิดพลาด กรุณาลองใหม่อีกครั้ง',
                        html: res.msg,
                        confirmButtonColor: '#d33',
                        confirmButtonText: 'ปิด',
                        allowOutsideClick: false,
                        focusConfirm: false,
                    });
                    return false;
                } else {
                    // $('form[id="form-set-payment"]').submit();
                    $(".ju-payment-create").html('');
                    $(".ju-payment-create").html(res.html);
                    LoadingHide();
                }
            }
        });
    }
</script>