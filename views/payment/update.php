<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\JuPayment */

$this->title = 'แก้ไข Ju Payment: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ju Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="ju-payment-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
