<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
// app\assets\PaymentAsset::register($this);

// echo '<pre>'; print_r($customerName); 
?>
<?php //header('Access-Control-Allow-Origin: *'); 
?>
<style>
    .mt-50 {
        margin-top: 50px
    }

    .mb-50 {
        margin-bottom: 50px
    }

    @media(max-width:767px) {
        .card {
            width: 90%;
            padding: 1.5rem
        }
    }

    @media(height:1366px) {
        .card {
            width: 90%;
            padding: 8vh
        }
    }

    .active {
        border-bottom: 2px solid black;
        font-weight: bold
    }

    input {
        border: none;
        outline: none;
        font-size: 1rem;
        font-weight: 600;
        color: #000;
        width: 100%;
        min-width: unset;
        background-color: transparent;
        border-color: transparent;
        margin: 0
    }

    form a {
        color: grey;
        text-decoration: none;
        font-size: 0.87rem;
        font-weight: bold
    }

    form a:hover {
        color: grey;
        text-decoration: none
    }

    form .row {
        overflow: hidden
    }

    form .row-1 {
        border: 1px solid rgba(0, 0, 0, 0.137);
        padding: 1.0rem;
        outline: none;
        width: 100%;
        min-width: unset;
        border-radius: 5px;
        background-color: rgba(221, 228, 236, 0.301);
        border-color: rgba(221, 228, 236, 0.459);
        margin: 2vh 0;
        overflow: hidden
    }

    form .row-2 {
        border: none;
        outline: none;
        background-color: transparent;
        margin: 0;
        padding: 0 0.8rem
    }

    form .row .row-2 {
        border: none;
        outline: none;
        background-color: transparent;
        margin: 0;
        padding: 0 0.8rem
    }

    form .row .col-2,
    .col-7,
    .col-3 {
        display: flex;
        align-items: center;
        text-align: center;
        padding: 0 1vh
    }

    form .row .col-2 {
        padding-right: 0
    }

    input:focus::-webkit-input-placeholder {
        color: transparent
    }

    input:focus:-moz-placeholder {
        color: transparent
    }

    input:focus::-moz-placeholder {
        color: transparent
    }

    input:focus:-ms-input-placeholder {
        color: transparent
    }
</style>
<div class="ju-payment-type-form">
    <div class="container">
        <div class="card-body">
            <div class="form-container">
                <div class="form-horizontal">
                    <h3 class="title">Credit Card (โปรดกรอกข้อมูล)</h3>

                    <!-- <div class="form-group">
                            <label for="ccnumber">ชื่อที่แสดงบนบัตร</label>
                            <input type="text" name="username" required class="form-control ">
                        </div>

                        <label for="ccnumber">หมายเลขบัตร</label>
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="0000 0000 0000 0000">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fa fa-credit-card"></i>
                                </span>
                            </div>
                        </div> -->
                    <!-- credit card info-->
                    <!-- <form role="form"> -->
                    <div class="form-group text-center">
                        <ul class="list-inline">
                            <li class="list-inline-item"><i class="text-muted fa fa-cc-visa fa-2x"></i></li>
                            <li class="list-inline-item"><i class="fa fa-cc-mastercard fa-2x"></i></li>
                            <li class="list-inline-item"><i class="fa fa-cc-amex fa-2x"></i></li>
                            <li class="list-inline-item"><i class="fa fa-cc-discover fa-2x"></i></li>
                        </ul>
                    </div>
                    <div class="form-group">
                        <label for="username">
                            <h5>ชื่อบนบัตร</h5>
                        </label>
                        <div type="text" class="form-control" placeholder="" required="" id="card-name"></div>
                    </div>
                    <div class="form-group">
                        <label for="cardNumber">
                            <h5>หมายเลขบัตร</h5>
                        </label>
                        <div class="input-group">
                            <div type="text" class="form-control" placeholder="" required="" id="card-number"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label data-toggle="tooltip">
                            <h5>วันทื่หมดอายุ</h5>
                        </label>
                        <div class="input-group">
                            <div type="text" class="form-control" placeholder="" required="" id="card-expiry">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label data-toggle="tooltip" title="รหัส CVV 3 หลักที่ด้านหลังบัตร">
                            <h5>CVV <i class="fa fa-question-circle d-inline"></i></h5>
                        </label>
                        <div class="input-group">
                            <div type="number" class="form-control" required="" id="card-cvv"></div>
                        </div>
                    </div>
                    <div>
                        <div id="remember-card"></div>
                    </div>
                    <div class="card-footer">
                        <div class="credit-form">
                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo Html::button('กลับ', ['class' => 'btn btn-warning btn-lg btn-block', 'onclick' => 'history.back(LoadingShow());']); ?>
                                </div>
                                <div class="col-md-6">
                                    <form method="POST" action="<?php echo Url::to(['payment/check-data-redirect']); ?>">
                                        <script src="https://dev-kpaymentgateway.kasikornbank.com/ui/v2/kinlinepayment.min.js" 
                                                data-apikey="<?php echo Yii::$app->params['publickey']; ?>" 
                                                data-lang="TH" 
                                                data-write-log="false">
                                        </script>
                                        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                                        <input type="hidden" name="customerName" value="<?=$resultData['customer_name'];?>" />
                                        <input type="hidden" name="amount" value="<?=$resultData['amount'];?>" />
                                        <input type="hidden" name="textSms" value="<?=$resultData['text_sms'];?>" />
                                        <input type="hidden" name="sourceType" value="<?=$sourceType;?>" />
                                        <button class="btn btn-success btn-lg btn-block"> ยืนยัน (<?=number_format($resultData['amount'], 2);?>) </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>