<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ju Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ju-payment-index">


    <?php $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],

                    [
                'attribute'=>'id',
                'vAlign'=>'middle',
                'hAlign'=>'left',
            ],
            [
                'attribute'=>'code_check',
                'vAlign'=>'middle',
                'hAlign'=>'left',
            ],
            [
                'attribute'=>'method',
                'vAlign'=>'middle',
                'hAlign'=>'left',
            ],
            [
                'attribute'=>'ref_id',
                'vAlign'=>'middle',
                'hAlign'=>'left',
            ],
            [
                'attribute'=>'amount',
                'vAlign'=>'middle',
                'hAlign'=>'left',
            ],
            // 'customer_name',
            // 'merchant_id',
            // 'merchant_name',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',

            ['class' => 'kartik\grid\ActionColumn',
                'template'=>'{view} {update}',//{delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="fa fa-eye"></span>', $url, [
                                    'title' => 'แสดง',
                                    'class' => 'btn btn-info btn-xs',
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="fa fa-edit"></span>', $url, [
                                    'title' => 'แก้ไข',
                                    'class' => 'btn btn-warning btn-xs',
                        ]);
                    },
                    // 'delete' => function ($url, $model) {
                    //     return Html::a('<span class="fa fa-trash"></span>', $url, [
                    //                 'title' => 'ลบ',
                    //                 'class' => 'btn btn-danger btn-xs',
                    //                 'data' => [
                    //                     'confirm' => 'คุณยืนยันการลบข้อมูลใช่หรือไม่',
                    //                     'data' => 'post',
                    //                 ],
                    //     ]);
                    // },
                ],
            ],
        ]; ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        //filterModel' => $searchModel,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=> '<i class="fa fa-list"></i>',
            'before'=> false,
            'after'=> false,
        ],
        'toolbar'=> [
            'content'=> ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'fontAwesome' => true,
                'target'=>ExportMenu::TARGET_BLANK,
                'exportConfig'=>[
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_PDF  => false,
                ],
                'filename'=>'export_data_'.date('Ymd'),
            ]),
            //'{toggleData}',
        ],
        'bordered' => true,
        'striped' => true,
        'responsive'=> false,
        'responsiveWrap' => false,
        'floatHeader'=>false,
        // 'floatOverflowContainer'=>true, //only set floatHeader = true for fix header
        'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false,        
        'headerRowOptions'=>['class'=>'info'],
        'filterRowOptions'=>['class'=>'info'],
        'columns' => $gridColumns,
    ]); ?>
</div>
