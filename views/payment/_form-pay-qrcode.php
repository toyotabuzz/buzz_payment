<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;
// app\assets\PaymentAsset::register($this);

// echo '<pre>'; print_r($customerName);
?>

<div id="content-qr-code"></div>
<script>
    var baseUrl = "<?php echo Url::base(); ?>";
    $(function() {
        getOrderId();
    });

    function getOrderId() {
        var customer_name = "<?= $resultData['customer_name']; ?>";
        var amount = "<?= $resultData['amount']; ?>";
        var text_sms = "<?= $resultData['text_sms']; ?>";
        var source_type = "<?= $sourceType; ?>";
        var order_id = "<?= $resultData['id']; ?>";

        $.ajax({
            type: 'POST',
            data: {
                customer_name: customer_name,
                amount: amount,
                text_sms: text_sms,
                source_type: source_type,
                order_id: order_id
            },
            dataType: 'json',
            url: baseUrl + '/payment/get-order-id',
            beforeSend: function() {
                LoadingShow();
            },
            success: function(res) {
                console.log(res);
                if (res.success) {
                    setTimeout(function() {
                        var pageFormPay = baseUrl + '/payment/get-button-qr-code?' + 'amount=' + res.amount + '&order_id=' + res.order_id;
                        window.location.href = pageFormPay;
                    }, 3000);
                } else {
                    console.log(res.message);
                }
            },
            complete: function(res) {
                LoadingHide();
            }
        }).fail(function(error) {
            Swal.fire({
                icon: 'error',
                title: 'เกิดข้อผิดพลาด กรุณาแจ้งผู้ดูแลระบบ (' + error.status + ')',
                html: '<p>payment Qr-code :</p><p>' + error.responseText + '</p>',
                confirmButtonColor: '#d33',
                confirmButtonText: 'ปิด',
                allowOutsideClick: false,
                focusConfirm: false,
            });
        });
    }
</script>