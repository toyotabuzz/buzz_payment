<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\JuPayment */

$this->title = 'เพิ่ม Ju Payment';
$this->params['breadcrumbs'][] = ['label' => 'Ju Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'เพิ่ม';
?>
<div class="ju-payment-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
