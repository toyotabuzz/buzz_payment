<?php

use yii\helpers\Url;
use yii\helpers\Html;

// \app\assets\PaymentAsset::register($this);
$this->title = 'ตรวจสอบการส่ง SMS';
// $this->params['breadcrumbs'][] = $this->title;
// echo '<pre>';
// print_r($resPayment);exit;
if ($resPayment['success']) {
    $bg = 'success';
} else {
    $bg = 'danger';
}
?>
<div class="payment-send-sms">
    <div class="container">
        <div class="row" style="margin-top: 5px;">
            <div class="col-sm-12">
                <div class="form-container">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">ตรวจสอบการส่ง SMS</h3>
                        </div>
                        <div class="panel-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <img src="<?php echo Yii::getAlias('@web'); ?>/img/payment/<?php echo $resPayment['icons']; ?>" width="90" height="90">
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-sm-12 text-center">
                                        <div class="text-<?php echo $bg; ?>" style="font-size: 18px; font-weight: 600; font-margin: 0;"><?php echo empty($resPayment['message']) ? null : $resPayment['message']; ?></div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-5 text-center">
                                        <button class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#detail-send-sms" aria-expanded="false"> แสดงรายละเอียด </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="collapse" id="detail-send-sms">
                                            <?php echo $this->render('_content-info-send-sms', ['resRequest' => $resRequest, 'resFullUrl' => $resFullUrl, 'resShortUrl' => $resShortUrl, 'resPayment' => $resPayment]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($resPayment['success']) { //ถ้ายังไม่ส่งให้เปิดปุ่ม 
                        ?>
                            <div class="panel-footer text-center">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?php echo Html::button('<i class="fa fa-paper-plane"></i> ส่ง SMS', ['class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin: 0px;', 'onclick' => 'sendSMS("' . $resRequest['customer_tel'] . '");']); ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $("input[class=swal2-input]").attr('disabled', true);
        // swal2-input
    });
    var baseUrl = "<?php echo Url::base(); ?>";

    function sendSMS(number) {
        var fnCheckNumber = phoneNumber(number);
        var fnCheckNumber = JSON.parse(fnCheckNumber);
        Swal.fire({
            icon: 'question',
            title: 'ต้องการส่ง SMS ใช่หรือไม่',
            // text: "เบอร์",
            showCancelButton: true,
            confirmButtonColor: '#00a65a',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก',
            showLoaderOnConfirm: true,
            allowOutsideClick: false,
            focusConfirm: false,
            inputValue: number,
            input: 'text',
            inputValidator: () => {
                return (!fnCheckNumber.success) && fnCheckNumber.message
            }
        }).then((result) => {
            if (result.value) {
                var resRequest = [];
                resRequest = JSON.parse('<?php echo json_encode($resRequest); ?>');
                resRequest['link_before_convert'] = '<?php echo $resFullUrl; ?>';
                resRequest['code_check'] = '<?php echo $resRequest['code_check']; ?>';
                resRequest['code_recheck'] = '<?php echo $resRequest['code_recheck']; ?>';
                // console.log(resRequest);return false;
                $.ajax({
                    url: baseUrl + '/payment/save-verify-send-sms',
                    type: 'get',
                    data: resRequest,
                    dataType: 'json',
                    beforeSend: function() {
                        LoadingShow();
                    },
                    success: function(data) {
                        console.log(data);
                        //บันทึกสำเร็จ
                        if (data.success) {
                            Swal.fire({
                                icon: 'success',
                                title: data.title + '<br><br>' + data.message,
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'ปิด',
                                allowOutsideClick: false,
                                focusConfirm: false,
                            }).then((result) => {
                                Swal.fire({
                                    title: 'กำลังปิด',
                                    text: 'โปรดรอสักครู่...',
                                    timer: location.reload(),
                                    allowOutsideClick: false,
                                    focusConfirm: false,
                                    onOpen: () => {
                                        swal.showLoading();
                                    }
                                })
                            });
                            //ไม่สำเร็จ
                        } else {
                            Swal.fire({
                                title: data.title,
                                text: data.message,
                                icon: 'error',
                                confirmButtonColor: '#d33',
                                confirmButtonText: 'ปิด',
                                allowOutsideClick: false,
                                focusConfirm: false,
                            }).then((result) => {
                                Swal.fire({
                                    title: 'กำลังปิด',
                                    text: 'โปรดรอสักครู่...',
                                    timer: location.reload(),
                                    allowOutsideClick: false,
                                    focusConfirm: false,
                                    onOpen: () => {
                                        swal.showLoading();
                                    }
                                })
                            });
                        }
                    },
                    complete: function() {
                        LoadingHide();
                    },
                }).fail(function(error) {
                    Swal.fire({
                        icon: 'error',
                        title: 'เกิดข้อผิดพลาด กรุณาแจ้งผู้ดูแลระบบ (' + error.status + ')',
                        html: '<p>paymentSaveSendSMS :</p><p>' + error.responseText + '</p>',
                        confirmButtonColor: '#d33',
                        confirmButtonText: 'ปิด',
                        allowOutsideClick: false,
                        focusConfirm: false,
                    });
                });
            }
        });
    }
</script>