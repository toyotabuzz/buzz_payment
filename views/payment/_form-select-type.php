<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'ตรวจสอบการชำระ';
// app\assets\PaymentAsset::register($this);
//echo '<pre>'; print_r($resultData); 
?>

<div class="ju-payment-type-form">
    <div class="card">
        <div class="container">
            <div class="card-body">
                <div class="form-container">
                    <form class="form-horizontal">
                        <h3 class="title">เลือกประเภทการชำระ</h3>
                        <div class="form-group">
                            <div class="col-sm-3" style="font-weight: bold;"> ชื่อร้าน :</div>
                            <div class="col-sm-9" style="padding-left: 25px;"><?php echo empty($resultData['merchant_name']) ? null : $resultData['merchant_name']; ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3" style="font-weight: bold;"> รหัสร้าน :</div>
                            <div class="col-sm-9" style="padding-left: 25px;"><?php echo empty($resultData['merchant_id']) ? null : $resultData['merchant_id']; ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3" style="font-weight: bold;"> ชื่อลูกค้า :</div>
                            <div class="col-sm-9" style="padding-left: 25px;"><?php echo empty($resultData['customer_name']) ? null : $resultData['customer_name']; ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3" style="font-weight: bold;"> จำนวนเงิน :</div>
                            <div class="col-sm-9" style="padding-left: 25px;"><?php echo empty($resultData['amount']) ? null : number_format($resultData['amount'], 2); ?></div>
                        </div>
                        <div class="payment-type">
                            <div class="types">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="type" data-id="1">
                                            <div class="logo">
                                                <img src="<?php echo Yii::getAlias('@web'); ?>/img/payment/credit.png" width="110" height="120">
                                            </div>
                                            <div class="text">
                                                <p>เครดิต</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="type" data-id="2">
                                            <div class="logo">
                                                <img src="<?php echo Yii::getAlias('@web'); ?>/img/payment/qr-code.png" width="110" height="120">
                                            </div>
                                            <div class="text">
                                                <p>QR-CODE</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <div class="type" data-id="3">
                                            <div class="logo">
                                                <img src="<?php echo Yii::getAlias('@web'); ?>/img/payment/alipay.png" width="110" height="120">
                                            </div>
                                            <div class="text">
                                                <p>Ali Pay</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo Html::button('เลือก', ['class' => 'btn btn-success btn-lg btn-block', 'onclick' => 'choosePaytype();']); ?>
                            </div>
                        </div>
                        <?php echo Html::hiddenInput('juPayment[id]', $resultData['id'], ['id' => 'jupayment-id']); ?>
                        <?php echo Html::hiddenInput('ref_order', $resultData['ref_order'], ['id' => 'ref-order']); ?>
                        <?php echo Html::hiddenInput('amount', $resultData['amount'], ['id' => 'amount']); ?>
                        <?php echo Html::hiddenInput('customer_name', $resultData['customer_name'], ['id' => 'customer-name']); ?>
                        <?php echo Html::hiddenInput('customer_tel', $resultData['customer_tel'], ['id' => 'customer-tel']); ?>
                        <?php echo Html::hiddenInput('merchant_id', $resultData['merchant_id'], ['id' => 'merchant-id']); ?>
                        <?php echo Html::hiddenInput('text_sms', $resultData['text_sms'], ['id' => 'text-sms']); ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var baseUrl = "<?php echo Url::base(); ?>";
    $(function() {
        $(".type").click(function() {
            $(".type").removeClass('selected');
            $(this).addClass("selected");
        });
    });

    function choosePaytype() {
        //1 = credit, 2 = qrcode, 3 = alipay
        //return false;
        var paymentId = $("#jupayment-id").val();
        var formId = $(".selected").data("id");
        var formId = $(".selected").data("id");
        var amount = $("#amount").val();
        var customerName = $("#customer-name").val();
        var customerTel = $("#customer-tel").val();
        var merchantId = $("#merchant-id").val();
        var textSms = $("#text-sms").val();
        // console.log(paymentId + '  -   ' + formId);
        //ถ้าเลือกประเภทการจ่าย
        if (paymentId && formId && amount && customerName && customerTel && merchantId) {
            //ดึงหัวการจ่าย
            $.getJSON(baseUrl + '/payment/get-data-formall', {
                formId: formId,
                paymentId: paymentId
            }, function(res) {
                if (res.success) { //ถ้ามีฟอร์มที่เลือก Ref
                    console.log(res)
                    Swal.fire({
                        icon: 'question',
                        title: 'ต้องการชำระเงินผ่าน <br><br>' + res.message,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'ยืนยัน',
                        cancelButtonText: 'ยกเลิก',
                        allowOutsideClick: false,
                        focusConfirm: false,
                    }).then((result) => {
                        if (result.value) {
                            // console.log(paymentId); return false;
                            LoadingShow();

                            var pageFormPay = baseUrl + '/payment/get-form-payment?' + 'payment_id=' + paymentId + '&link=' + res.link + '&source_type=' + res.sourceType;
                            window.location.href = pageFormPay;
                        }

                    });
                } else { //ไม่พบข้อมูลใน Ref
                    Swal.fire({
                        icon: 'warning',
                        title: 'พบข้อผิดพลาด <br><br>' + res.message,
                        confirmButtonColor: '#d33',
                        confirmButtonText: 'ปิด',
                        allowOutsideClick: false,
                        focusConfirm: false,
                    })
                }
            });
        } else {
            Swal.fire({
                icon: 'warning',
                title: 'กรุณาเลือกช่องทางการชำระ',
                // html: msg,
                confirmButtonColor: '#d33',
                confirmButtonText: 'ปิด',
                allowOutsideClick: false,
                focusConfirm: false,
            });
            return false;
        }
    }
</script>