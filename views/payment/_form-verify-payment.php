<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

// \app\assets\PaymentAsset::register($this);
$this->title = 'ตรวจสอบการชำระ';
?>
<?php //echo '<pre>'; print_r($resultData); exit;
?>
<style>
    @media (min-width:992px) {
        .page-container {
            max-width: 1140px;
            margin: 0 auto
        }

        .page-sidenav {
            display: block !important
        }
    }

    .padding {
        padding: 2rem
    }

    .w-32 {
        width: 32px !important;
        height: 32px !important;
        font-size: .85em
    }

    .tl-item .avatar {
        z-index: 2
    }

    .circle {
        border-radius: 500px
    }

    .gd-warning {
        color: #fff;
        border: none;
        background: #f4c414 linear-gradient(45deg, #f4c414, #f45414)
    }

    .timeline {
        position: relative;
        border-color: rgba(160, 175, 185, .15);
        padding: 0;
        margin: 0
    }

    .p-4 {
        padding: 1.5rem !important
    }

    .mb-4,
    .my-4 {
        margin-bottom: 1.5rem !important
    }

    .tl-item {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        /* border-bottom: 2px solid #e7e7e7; */
        border-width: thin;
        padding-bottom: 10px;
        border-color: #ff440024;
    }

    .tl-item>* {
        padding: 10px
    }

    .tl-item .avatar {
        z-index: 2
    }

    .tl-item:last-child .tl-dot:after {
        display: none
    }

    .tl-dot {
        /* position: relative; */
        border-color: rgba(160, 175, 185, .15)
    }

    .tl-dot::after,
    .tl-dot::before {
        content: "";
        display: block;
        box-sizing: border-box;
        position: absolute;
        bottom: -2px;
        margin-left: 23px;
        color: #1460bf;
    }

    .tl-dot::after {
        width: 9px;
        height: 9px;
        border-bottom: 3px solid;
        border-left: 3px solid;
        transform: rotate(-45deg);
        left: 6.3px
    }

    .tl-dot::before {
        width: 2px;
        height: 16px;
        left: 10px;
        background: currentColor
    }

    .tl-content p:last-child {
        margin-bottom: 0
    }

    .tl-date {
        font-size: .85em;
        margin-top: 2px;
        min-width: 100px;
        max-width: 200px
    }

    .avatar {
        position: relative;
        line-height: 1;
        border-radius: 500px;
        white-space: nowrap;
        font-weight: 700;
        border-radius: 100%;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-pack: center;
        justify-content: center;
        -ms-flex-align: center;
        align-items: center;
        -ms-flex-negative: 0;
        flex-shrink: 0;
        border-radius: 500px;
        box-shadow: 0 5px 10px 0 rgba(50, 50, 50, .15);
        width: 50px !important;
        height: 50px !important;
    }
</style>
<div class="ju-payment-type-form">
    <div class="card">
        <div class="container">
            <div class="card-body">
                <!-- <div class="row"> -->
                <!-- <div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6"> -->
                <div class="form-container">
                    <form class="form-horizontal">
                        <!-- <h3 class="title"><?php //echo empty($resultError) ? null : $resultError; 
                                                ?></h3> -->
                        <div class="payment-type">
                            <div class="types">
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <img src="<?php echo Yii::getAlias('@web'); ?>/img/payment/<?php echo $resultIcon; ?>" width="90" height="90">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <div style="font-size: 18px; color: #499649; font-weight: 600; font-margin: 0;"><?php echo empty($resultError) ? null : $resultError; ?></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <div style="margin: 0;">รหัสอ้างอิง <?php echo empty($resultData['ref_no']) ? null : $resultData['ref_no']; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tl-item active" id="round-vertically">
                            <div class="tl-dot"><span class="w-32 avatar circle gd-warning"><img src="<?php echo Yii::getAlias('@web'); ?>/img/payment/icon-owner.png" width="35" height="35" style="margin-top: -5px;"></span></div>
                            <div class=" tl-content">
                                    <div class=""><b>ชื่อลูกค้า</b> <?php echo empty($resultData['customer_name']) ? "ไม่พบข้อมูล" : $resultData['customer_name']; ?></div>
                                    <div class="tl-date text-muted mt-1"><?php echo empty($resultData['date_pay_complete']) ? "ไม่พบข้อมูล" :  date('d-m-Y H:i', strtotime($resultData['date_pay_complete'])) . ' น.'; ?></div>
                            </div>
                        </div>
                        <div class="tl-item">
                            <div class=""><span class="w-32 avatar circle gd-warning"><img src="<?php echo Yii::getAlias('@web'); ?>/img/payment/icon-buzz.png" width="50" height="50""></span></div>
                            <div class=" tl-content">
                                    <div class=""><b>ชื่อร้านค้า</b> <?php echo empty($resultData['merchant_name']) ? "ไม่พบข้อมูล" : $resultData['merchant_name']; ?> </div>
                                    <div class="tl-date text-muted mt-10">รหัสร้านค้า <?php echo empty($resultData['merchant_id']) ? "ไม่พบข้อมูล" : $resultData['merchant_id']; ?> </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group" style="margin-top: -17px;">
                            <div class="col-sm-3" style="font-weight: bold;"> วันที่ทำรายการ :</div>
                            <div class="col-sm-6" style="padding-left: 25px;"><?php echo empty($resultData['date_pay_complete']) ? "ไม่พบข้อมูล" :  date('d-m-Y H:i', strtotime($resultData['date_pay_complete'])); ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3" style="font-weight: bold;"> ชื่อลูกค้า :</div>
                            <div class="col-sm-9" style="padding-left: 25px;"><?php echo empty($resultData['customer_name']) ? "ไม่พบข้อมูล" : $resultData['customer_name']; ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3" style="font-weight: bold;"> จำนวนเงิน :</div>
                            <div class="col-sm-9" style="padding-left: 25px;"><?php echo empty($resultData['amount']) ? "ไม่พบข้อมูล" : number_format($resultData['amount'], 2); ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3" style="font-weight: bold;"> ชื่อร้านค้า :</div>
                            <div class="col-sm-9" style="padding-left: 25px;"><?php echo empty($resultData['merchant_name']) ? "ไม่พบข้อมูล" : $resultData['merchant_name']; ?></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3" style="font-weight: bold;"> รหัสร้านค้า :</div>
                            <div class="col-sm-9" style="padding-left: 25px;"><?php echo empty($resultData['merchant_id']) ? "ไม่พบข้อมูล" : $resultData['merchant_id']; ?></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo Html::button('กลับหน้าหลัก', ['class' => 'btn-success btn-lg btn-block', 'onclick' => 'closePage();']); ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>

<script>
    function closePage() {
        window.open('', '_self', ''); //bug fix
        window.close();
    }
</script>