<?php 
//ถ้าส่ง SMS ได้
if ($resPayment['success']) {
    $bg = 'success';
} else {
    $bg = 'danger';
}
?>
<div class="form-info-send-sms" style="margin-top: 10px;">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <?php
            if (!empty($resRequest)) {
            ?>
                <div class="alert alert-<?php echo $bg; ?>" style="padding: 5px">
                    <div class="row">
                        <div class="col-sm-3" style="font-weight: bold;">METHOD :</div>
                        <div class="col-sm-9"><?php echo empty($resRequest['method']) ? '-' : $resRequest['method']; ?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" style="font-weight: bold;">REF_ID :</div>
                        <div class="col-sm-9"><?php echo empty($resRequest['ref_id']) ? '-' : $resRequest['ref_id']; ?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" style="font-weight: bold;">ชื่อลูกค้า :</div>
                        <div class="col-sm-9"><?php echo empty($resRequest['customer_name']) ? '-' : $resRequest['customer_name']; ?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" style="font-weight: bold;">เบอร์โทรศัพท์ :</div>
                        <div class="col-sm-9"><?php echo empty($resRequest['customer_tel']) ? '-' : $resRequest['customer_tel']; ?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" style="font-weight: bold;">จำนวนเงิน :</div>
                        <div class="col-sm-9"><?php echo empty($resRequest['amount']) ? '-' : number_format($resRequest['amount'], 2); ?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" style="font-weight: bold;">ไปยัง :</div>
                        <div class="col-sm-9"><?php echo empty($resRequest['merchant_name']) ? '-' : $resRequest['merchant_name']; ?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" style="font-weight: bold;">CODE_CHECK :</div>
                        <div class="col-sm-9" style=" word-break: break-word;"><?php echo empty($resRequest['code_check']) ? '-' : $resRequest['code_check']; ?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" style="font-weight: bold;">CODE_RECHECK :</div>
                        <div class="col-sm-9" style=" word-break: break-word;"><?php echo empty($resRequest['code_recheck']) ? '-' : $resRequest['code_recheck']; ?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" style="font-weight: bold;">URL_FULL :</div> 
                        <div class="col-sm-9" style=" word-break: break-word;"><?php echo empty($resFullUrl) ? '-' : $resFullUrl; ?></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" style="font-weight: bold;">TEXT_SMS :</div>
                        <div class="col-sm-9"><?php echo empty($resRequest['text_sms']) ? '-' : $resRequest['text_sms']; ?></div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="row" style="font-size: 8px; word-break: break-word;">
        <div class="col-sm-6">
            <?php // ?>
        </div>
    </div>
</div>